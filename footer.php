
	<section class="logos">
		<div>
			<a href="http://swedenbybike.com/" class="swedenbybike">Sweden by Bike</a>
			<div class="in-cooperation-with"><?php echo esc_html__( 'i samarbete med', 'bagagetransport' ); ?></div>
			<a href="https://jetpak.com/se" class="jetpak">Jetpak</a>
		</div>
	</section>

	<footer class="bottom">
		<?php echo esc_html__( 'Bagagetjänsten tillhandahålls av', 'bagagetransport' ); ?> <a href="http://swedenbybike.com" class="nobreak">Sweden by Bike AB</a> <?php echo esc_html__( 'i samarbete med', 'bagagetransport' ); ?> <a href="https://jetpak.com/se" class="nobreak">Jetpak Sverige AB</a>.
		<div class="wrap help"><?php echo esc_html__( 'Vid frågor eller manuell bokning kontakta oss gärna på 08-640 96 56 eller', 'bagagetransport' ); ?> <a href="mailto:&#107;&#117;&#110;&#100;&#115;&#101;&#114;&#118;&#105;&#99;&#101;&#64;&#98;&#97;&#103;&#97;&#103;&#101;&#116;&#114;&#97;&#110;&#115;&#112;&#111;&#114;&#116;&#46;&#115;&#101;">&#107;&#117;&#110;&#100;&#115;&#101;&#114;&#118;&#105;&#99;&#101;&#64;&#98;&#97;&#103;&#97;&#103;&#101;&#116;&#114;&#97;&#110;&#115;&#112;&#111;&#114;&#116;&#46;&#115;&#101;</a></div>
		<div class="wrap copy"><span class="nobreak">&copy; <?php echo date("Y"); ?> <a href="http://swedenbybike.com" class="nobreak">Sweden by Bike AB</a></span> &bull; <span class="nobreak">Centralplan 3</span> &bull; <span class="nobreak">111 20 Stockholm</span> &bull; <a href="mailto:&#105;&#110;&#102;&#111;&#64;&#115;&#119;&#101;&#100;&#101;&#110;&#98;&#121;&#98;&#105;&#107;&#101;&#46;&#99;&#111;&#109;">&#105;&#110;&#102;&#111;&#64;&#115;&#119;&#101;&#100;&#101;&#110;&#98;&#121;&#98;&#105;&#107;&#101;&#46;&#99;&#111;&#109;</a></div>
	</footer>

	<section id="help" class="hidden">
		<div class="wrap">
			<div class="help-close top"><button class="button small">-</button></div>
			<section class="help-faq">
				<?php
					$faq_sections = array(
						esc_html__( 'Priser', 'bagagetransport' ) => array(
							esc_html__( 'Vad kostar bagagetransport?', 'bagagetransport' ) => esc_html__( 'Priset beror på hur långt bagaget ska transporteras och hur mycket det väger. I grundpriset ingår 50 kg bagage, oavsett antal väskor (eller antal personer). Du kan betala tillägg för att transportera ända upp till 300 kg.', 'bagagetransport' ),

							esc_html__( 'Vad blir kostnaden om mitt bagage väger mer än 50 kg?', 'bagagetransport' ) => esc_html__( 'Om ditt bagage väger mer än 50 kg kan du betala ett tillägg för bagage upp till 300 kg. Det gör du i samband med din bokning där du också får en prisuppgift direkt. Om ni är en större grupp med en total bagagevikt över 300 kg ber vi er kontakta oss för prisuppgift.', 'bagagetransport' ),
						),
						esc_html__( 'Min bokning', 'bagagetransport' ) => array(
							esc_html__( 'Hur gör jag för att boka?', 'bagagetransport' ) => esc_html__( 'Du bokar och betalar din bagagetransport online. Inom 48 timmar får du en bekräftelse från oss som innehåller ditt 11-siffriga bokningsnummer. Informationen skickas till den mailadress som du uppgav vid bokningstillfället.', 'bagagetransport' ),

							esc_html__( 'Hur sent inpå kan jag göra en bokning?', 'bagagetransport' ) => esc_html__( 'Det är säkrast att ringa oss om du vill boka bagagetransport med kort varsel (mindre än 2 dagar i förväg).  Du når oss mån-fre kl. 08:00-17:00 på tel 08-640 96 56.', 'bagagetransport' ),
						),
						esc_html__( 'Ändra och avboka', 'bagagetransport' ) => array(
							esc_html__( 'Får jag pengarna tillbaka om jag avbokar?', 'bagagetransport' ) => esc_html__( 'Vid avbokning mer än 48 timmar innan inbokat upphämtningsdatum återbetalas hela beloppet. Vid avbokning mindre än 48 timmar återbetalas 50% av summan.', 'bagagetransport' ),

							esc_html__( 'Vad gäller vid sen avbokning (mindre än 48 h innan)?', 'bagagetransport' ) => esc_html__( 'För att göra en ändring eller avbokning mindre än 48 timmar innan bokad transport måste du ringa oss direkt. Du når oss alla dagar dygnet runt på tel 077-570 00 00. Uppge alltid ditt bokningsnummer när du tar kontakt.', 'bagagetransport' ),
						),
						esc_html__( 'Mitt bagage', 'bagagetransport' ) => array(
							esc_html__( 'Hur ska jag märka mitt bagage?', 'bagagetransport' ) => esc_html__( 'Du märker själv upp bagaget i förväg. Viktigast av allt är ditt 11-siffriga bokningsnummer (som du fick i samband med att du bokade). Märk upp varje väska med texten " Jetpak" + ditt 11-siffriga bokningsnummer, datum när bagaget ska transporteras samt ditt namn och mobilnummer.', 'bagagetransport' ),

							esc_html__( 'Kan ni transportera specialbagage?', 'bagagetransport' ) => esc_html__( 'Vi transporterar även din cykel, golfbag, barnvagn eller dina skidor. Skriv bara in det som extra information till chauffören vid hämtning.', 'bagagetransport' ),

							esc_html__( 'Hur fungerar hämtning och lämning av mitt bagage?', 'bagagetransport' ) => esc_html__( 'Du lämnar ditt bagage före kl.10 i receptionen på det hotell där du övernattat. Jetpaks chaufför hämtar bagaget och levererar det senast kl.16 i receptionen på det hotell eller vandrarhem där du ska övernatta. Vi anger normalt inte någon mer exakt tidpunkt för hämtning/lämning av ditt bagage än så, men om du behöver en mer precis tid kan vi ordna det mot ett tillägg i samband med din bokning.   Du får ett sms när bagaget är levererat. Om du kommer med flyg så hämtar/lämnar du ditt bagage hos Jetpak på flygplatsen. Du får mer information i samband med din bokning. Du kan också välja att lämna bagaget i en förvaringsbox på en tågstation. Då meddelar du bara oss aktuellt boxnummer och den 9-siffriga box-koden så att Jetpaks chaufför kan öppna boxen och hämta ditt bagage. Förvaringsboxarna finns i olika modeller och storlekar på ett antal orter. Här hittar du mer information. Tänk på att betalningsmöjligheter för boxarna varierar med kort respektive mynt varierar. Vi kan även hämta upp ditt bagage på valfri adress (kanske hemma eller på jobbet), men då är det viktigt att någon finns på plats som kan lämna ut respektive ta emot ditt bagage. ', 'bagagetransport' ),

							esc_html__( 'Är mitt bagage försäkrat?', 'bagagetransport' ) => esc_html__( 'Ditt bagage är automatiskt försäkrat enligt de regler som gäller för NSAB2000. Försäkringen gäller utan självrisk.', 'bagagetransport' ),
						),
						esc_html__( 'Hjälp och kontakt', 'bagagetransport' ) => array(
							esc_html__( 'Hur kontaktar jag kundtjänst?', 'bagagetransport' ) => esc_html__( 'Det är enkelt att komma i kontakt med vår kundtjänst via telefon 08-640 96 56. Kundtjänsten har öppet mån-fre kl. 08:00-17:00. För att göra en ändring eller avbokning mindre än 48 timmar innan bokad transport måste du ringa Jetpak på tel 077-570 00 00. De har öppet alla dagar dygnet runt. Om ditt ärende inte är brådskande kan du också skicka ett mail till kundservice@bagagetransport.se Uppge alltid ditt 11-siffriga bokningsnummer när du tar kontakt.', 'bagagetransport' ),

							esc_html__( 'Mitt bagage är försenat, skadat eller förlorat - vad gör jag?', 'bagagetransport' ) => esc_html__( 'Vi hanterar ditt bagage med största omsorg. Ibland händer det ändå saker som vi inte rår över. Om ditt bagage skulle vara skadat eller försenat tar du kontakt direkt med Jetpaks kundtjänst på tel 077-570 00 00. Uppge alltid ditt 11-siffriga bokningsnummer när du tar kontakt.', 'bagagetransport' ),

							esc_html__( 'Vad är Jetgaranti?', 'bagagetransport' ) => esc_html__( 'När du bokar din bagagetransport ingår Jetgaranti. Det innebär att du får pengarna tillbaka om bagaget är mer än en timme försenat, jämfört med den leveranstid som utlovats vid bokningstillfället.', 'bagagetransport' ),
						),
					);
				?>
				<h2 class="aligncenter"><?php echo esc_html__( 'Frågor och svar', 'bagagetransport' ); ?></h2>
				<div class="help-faq-questions">
				<?php
				$index = 1;
				foreach ($faq_sections as $key => $value) { ?>
					<div>
						<h3><?php echo $key; ?></h3>
						<ul>
							<?php
							foreach ($value as $faq_key => $faq_value) { ?>
							    <li>
							    	<a href="#help-faq-<?php echo $index; ?>"><?php echo $faq_key; ?></a>
							    	<div class="answer"><?php echo $faq_value; ?></div>
							    </li>
							    <?php
							    $index++;
							}
							?>
						</ul>
					</div>
				    <?php
				}
				?>
				</div>
			</section>
			<div class="help-close bottom"><button class="button small"><?php echo esc_html__( 'Stäng', 'bagagetransport' ); ?></button></div>
		</div>
	</section>

<?php wp_footer(); ?>

<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</body>
</html>