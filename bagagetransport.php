<?php
/**
 * Template Name: Bagagetransport One Page Checkout
 *
 * @package Bagagetransport
 */
?>

<?php get_header(); ?>

	<section id="instructions">
		<div class="wrap">
			<h2 class="aligncenter"><?php echo esc_html__( 'Gör så här', 'bagagetransport' ); ?></h2>
			<ol>
				<li><i class="material-icons">location_on</i><?php echo esc_html__( 'Fyll i önskad sträcka i formuläret nedan för att se ditt pris', 'bagagetransport' ); ?></li>
				<li><i class="material-icons">create</i><?php echo esc_html__( 'Fyll i dina kontaktuppgifter', 'bagagetransport' ); ?></li>
				<li><i class="material-icons">credit_card</i><?php echo esc_html__( 'Betala enkelt och smidigt med ditt betalkort via vår säkra anslutning*', 'bagagetransport' ); ?></li>
				<li><i class="material-icons">schedule</i><?php echo esc_html__( 'Inom 48 timmar får du en bekräftelse till den epostadress som uppgets i samband med bokningen', 'bagagetransport' ); ?></li>
				<li><i class="material-icons">label</i><?php echo esc_html__( 'Märk upp ditt bagage med det 11-siffriga bokningsnummer som följer med bekräftelsen', 'bagagetransport' ); ?></li>
				<li><i class="material-icons">work</i><?php echo esc_html__( 'Bagaget hämtas upp innan klockan 10:00 och levereras senast klockan 16:00 samma dag', 'bagagetransport' ); ?></li>
			</ol>
			<div class="asterix"><?php echo esc_html__( '* Framkörningsavgift kan tillkomma, du meddelas om detta i sådant fall direkt efter genomförd bokning. Om du ångrar dig eller tjänsten inte kan levereras av någon anledning återbetalas hela beloppet. Se frågor och svar för mer information kring avbokning.', 'bagagetransport' ); ?></div>
		</div>
	</section>

	<section id="itineraries" class="itineraries group">
		<div class="ribbon"><span><?php echo esc_html__( 'Från 250 sek', 'bagagetransport' ); ?></span></div>
		<div class="wrap">
			<?php show_cart_products(); ?>

			<div class="group actions">
				<button id="addItinierary" class="button nobreak"><?php echo esc_html__( 'Lägg till sträcka', 'bagagetransport' ); ?></button>
				<?php /*<button id="clearCart" class="button">Rensa allt</button> */ ?>
			</div>
		</div>
	</section>

	<section id="content">
		<?php 
			$cart_total = WC()->cart->cart_contents_total;
			$tax_total = WC()->cart->tax_total;
		?>
		<div id="cart_total" class="total price"><div class="wrap"><?php echo esc_html__( 'Totalt pris', 'bagagetransport' ); ?>: <span class="cart_total"><?php echo ($cart_total + $tax_total); ?></span> kr<?php if($cart_total > 1) { echo ' <span class="tax_total">(inkl <span class="amount">'.$tax_total.'</span> kr moms)</span>'; } ?></div></div><?php /*

		<div id="customer-select" class="customer-select">
			<h2><?php _e('Beställer du som privatperson eller företag?'); ?></h2>
			<button class="button select-customer-type for-private selected"><?php echo esc_html__( 'Privatperson', 'bagagetransport' ); ?></button>
			<button class="button select-customer-type for-business"><?php echo esc_html__( 'Företag', 'bagagetransport' ); ?></button> */ ?>
		</div>

		<div id="checkout">
			<div class="wrap">
				<?php echo do_shortcode('[woocommerce_checkout]'); ?>
				<?php /*echo do_shortcode('[woocommerce_cart]');*/ ?>
			</div>
		</div>

		 <?php
		while ( have_posts() ) : the_post(); ?>
		    <div class="entry-content-page">
		        <?php the_content(); ?>
		    </div>

		<?php
		endwhile;
		wp_reset_query();
		?>
	</section>

<?php get_footer(); ?>
