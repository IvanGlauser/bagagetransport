<?php
	// Custom login style
	add_action('login_head', 'custom_login');
	
	function custom_login() { 
		echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('template_directory').'/custom-login/custom-login.css" />'; 
	}
	
	// Custom login logo link
	function wpc_url_login(){
		return "http://landarkitektur.se";
	}
	add_filter('login_headerurl', 'wpc_url_login');
?>
<?php
	// Remove links from admin
	add_action('admin_menu', 'my_remove_menu_pages');
	function my_remove_menu_pages() {
		remove_menu_page('link-manager.php');	
	}
?>
<?php
	// Remove comments from admin menu
	add_action( 'admin_menu', 'my_remove_admin_menus' );
	function my_remove_admin_menus() {
		remove_menu_page( 'edit-comments.php' );
	}
	
	// Remove comments from post and pages
	add_action('init', 'remove_comment_support', 100);
	function remove_comment_support() {
		remove_post_type_support( 'post', 'comments' );
		remove_post_type_support( 'page', 'comments' );
	}
	
	// Remove comments from admin bar
	function mytheme_admin_bar_render() {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('comments');
	}
	add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );
	
	// Remove post metaboxes
	function remove_extra_meta_boxes() {
		remove_meta_box( 'postcustom' , 'post' , 'normal' ); // custom fields for posts
		remove_meta_box( 'postcustom' , 'page' , 'normal' ); // custom fields for pages
		remove_meta_box( 'postexcerpt' , 'post' , 'normal' ); // post excerpts
		remove_meta_box( 'postexcerpt' , 'page' , 'normal' ); // page excerpts
		remove_meta_box( 'commentsdiv' , 'post' , 'normal' ); // recent comments for posts
		remove_meta_box( 'commentsdiv' , 'page' , 'normal' ); // recent comments for pages
		//remove_meta_box( 'tagsdiv-post_tag' , 'post' , 'side' ); // post tags
		//remove_meta_box( 'tagsdiv-post_tag' , 'page' , 'side' ); // page tags
		remove_meta_box( 'trackbacksdiv' , 'post' , 'normal' ); // post trackbacks
		remove_meta_box( 'trackbacksdiv' , 'page' , 'normal' ); // page trackbacks
		remove_meta_box( 'commentstatusdiv' , 'post' , 'normal' ); // allow comments for posts
		remove_meta_box( 'commentstatusdiv' , 'page' , 'normal' ); // allow comments for pages
		remove_meta_box('slugdiv','post','normal'); // post slug
		remove_meta_box('slugdiv','page','normal'); // page slug
	}
	add_action( 'admin_menu' , 'remove_extra_meta_boxes' );
?>
<?php
	// Create the function to use in the action hook
	function remove_dashboard_widgets() {
		// Globalize the metaboxes array, this holds all the widgets for wp-admin
		global $wp_meta_boxes;
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	} 

	// Hoook into the 'wp_dashboard_setup' action to register our function
	add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
	
	// Hide welcome panel
	function hide_welcome_panel() {
		$user_id = get_current_user_id();

		if ( 1 == get_user_meta( $user_id, 'show_welcome_panel', true ) )
			update_user_meta( $user_id, 'show_welcome_panel', 0 );
	}
	add_action( 'load-index.php', 'hide_welcome_panel' );
?>
<?php
	// Custom dashboard help
	add_action( 'wp_dashboard_setup', 'my_dashboard_setup_function' );
	function my_dashboard_setup_function() {
		add_meta_box( 'custom_help_widget', 'Help and Support', 'custom_dashboard_help', 'dashboard', 'side', 'high' );
	}
	
	function custom_dashboard_help() {
	   echo '<p>Welcome to admin pages of landarkitektur.se! Lost or need any help? Contact <a href="http://chamigo.se">Chamigo</a>.</p>';
	}
?>
<?php
	// Custom admin footer
	add_filter('admin_footer_text', 'remove_footer_admin');
	
	function remove_footer_admin () {
    echo 'Powered by <a href="http://www.wordpress.org">WordPress</a> | <a href="http://codex.wordpress.org/">Documentation</a> | <a href="http://wordpress.org/support/forum/4">Feedback</a> | Land Arkitektur theme developed by <a href="http://chamigo.se">Chamigo AB</a></p>';
    }
?>