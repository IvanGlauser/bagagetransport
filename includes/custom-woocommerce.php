<?php
	add_action('after_setup_theme', 'woocommerce_support');
	function woocommerce_support() {
    	add_theme_support( 'woocommerce' );
	}

	add_filter( 'woocommerce_cart_needs_shipping_address', '__return_true', 50 );
?>
<?php
	/**
	* Preview WooCommerce Emails.
	* @author WordImpress.com
	* @url https://github.com/WordImpress/woocommerce-preview-emails
	* If you are using a child-theme, then use get_stylesheet_directory() instead
	*/

	$preview = get_stylesheet_directory() . '/woocommerce/emails/woo-preview-emails.php';

	if(file_exists($preview)) {
	    require $preview;
	}
?>
<?php
	// Modified checkout fields
	add_filter( 'woocommerce_billing_fields', 'custom_override_default_billing_fields', 10, 1 );
	function custom_override_default_billing_fields( $fields = array() ) {
		unset($fields['billing_address_1']);
		unset($fields['billing_address_2']);
		unset($fields['billing_state']);
	 	unset($fields['billing_city']);
	 	unset($fields['billing_postcode']);
	 	unset($fields['billing_company']);

		return $fields;
	}

		// Modified checkout fields
	add_filter( 'woocommerce_shipping_fields', 'custom_override_default_shipping_fields', 10, 1 );
	function custom_override_default_shipping_fields( $fields = array() ) {
	 	unset($fields['shipping_company']);
	 	unset($fields['shipping_address_1']);
	 	unset($fields['shipping_address_2']);
	 	unset($fields['shipping_city']);
	 	unset($fields['shipping_postcode']);
	 	unset($fields['shipping_country']);
	 	unset($fields['shipping_state']);

		return $fields;
	}



	// Special price for this product - check add-ons
	function calculate_custom_price($cart_object) {
        foreach ( $cart_object->get_cart() as $cart_item ) {
        	$price = get_product_price($cart_item);
        	$cart_item['data']->set_price($price);
        }

	}
	add_action('woocommerce_before_calculate_totals', 'calculate_custom_price', 1, 1);

	function get_product_price($product) {
		$date = $product['Datum'];
        $distance = $product['Avstånd'];

        $price_by_zone = get_price_by_zone($distance);
        $price_extra_by_distance = get_price_extra_by_distance($distance);
        //$product_variant_price = $cart_item['data']->get_price();

        $price = $price_by_zone + $price_extra_by_distance;

        $price = price_modify_by_date($date, $price);
        
        return $price;
	}


	/**
	* Add the field to the checkout
	**/
	add_action( 'woocommerce_before_order_notes', 'wordimpress_custom_checkout_field' );

	function wordimpress_custom_checkout_field( $checkout ) {

		echo '<div id="my_custom_checkout_field"><h3>' . __( 'Book Customization' ) . '</h3><p style="margin: 0 0 8px;">Would you like an inscription from the author in your book?</p>';

		woocommerce_form_field( 'inscription_checkbox', array(
		'type'  => 'checkbox',
		'class' => array( 'inscription-checkbox form-row-wide' ),
		'label' => __( 'Yes' ),
		), $checkout->get_value( 'inscription_checkbox' ) );

		woocommerce_form_field( 'inscription_textbox', array(
		'type'  => 'text',
		'class' => array( 'inscription-text form-row-wide' ),
		'label' => __( 'To whom should the inscription be made?' ),
		), $checkout->get_value( 'inscription_textbox' ) );

		echo '</div>';

	}


?>