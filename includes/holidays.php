<?php

    class Holidays {

        private $dates;
        private $enum_dates = array(
            'NEW_YEARS_DAY' => array('Nyårsdagen'),
            'EPIPHANY' => array('Trettondedag jul', 'Trettondagen'),
            'HOLY_THURSDAY' => array('Skärtorsdagen'),
            'GOOD_FRIDAY' => array('Långfredag'),
            'EASTER_EVE' => array('Påskafton'),
            'EASTER_DAY' => array('Påskdagen'),
            'EASTER_MONDAY' => array('Annandag påsk'),
            'WALPURGIS_NIGHT ' => array('Valborgsmässoafton'),
            'MAY_FIRST' => array('Första maj', '1:a maj'),
            'ASCENSION_OF_JESUS' => array('Kristi himmelsfärdsdag', 'Kristi H-dag'),
            'PENTECOST' => array('Pingstdagen'),
            'NATIONAL_DAY_OF_SWEDEN' => array('Sveriges nationaldag', 'Nationaldagen'),
            'MIDSUMMER_EVE' => array('Midsommarafton'),
            'MIDSUMMER_DAY' => array('Midsommardagen'),
            'ALL_SAINTS_DAY' => array('Alla helgons dag'),
            'CHRISTMAS_EVE' => array('Julafton'),
            'CHRISTMAS_DAY' => array('Juldagen'),
            'CHRISTMAS_DAY_DAY' => array('Annandag jul'),
            'NEW_YEARS_EVE' => array('Nyårsafton')
        );

        function __constructor($year = null) {

            $year = $year == null ? date('Y') : $year;
            $dates = self::getHolidays($year);

            $arr = array();

            foreach($this->enum_dates as $enum => $var) {
                $arr[$enum] = (object)array(
                    'enum' => $enum,
                    'date' => $dates->$enum,
                    'variations' => $var
                );
            }

            $this->dates = $arr;
        }

        public static function isHoliday($date = null) {

            $date = $date == null ? date('Y-m-d') : $date;
            $year = date('Y', strtotime($date));

            $dates = self::getHolidays($year);

            foreach($dates as $enum => $day) {
                //if($date == $day->date) {
                if($date == $day) {
                    return $day;
                }
            }

            return false;
        }

        public static function getHolidays($year) {
            $day_seconds = 60 * 60 * 24;
            $easter_sunday = easter_date($year);

            // Långfredag
            $i = 0;
            while(!isset($good_friday)) {
                $i++;
                $day = $easter_sunday - ($i * $day_seconds);

                if(date('N', $day) == 5) {
                    $good_friday = $day;
                }
            }

            // Midsommardagen
            for($i = $year . '0620'; $i < $year . '0626'; $i++) {
                if(date('N', strtotime($i)) == 6) {
                    $midsummer_day = strtotime($i);
                }
            }

            // Alla helgon
            for($i = $year . '1101'; $i < $year . '1106'; $i++) {
                if(date('N', strtotime($i)) == 6) {
                    $all_saints_day = strtotime($i);
                }
            }
            if(!isset($all_saints_day)) {
                $all_saints_day = strtotime($year . '-10-31');
            }

            return array(
                'NEW_YEARS_DAY' => $year . '-01-01', // Nyårsdagen
                'EPIPHANY' => $year . '-01-06', // Trettondedag jul
                'HOLY_THURSDAY' => date('Y-m-d', $good_friday - (1 * $day_seconds)), // Skärtorsdagen
                'GOOD_FRIDAY' => date('Y-m-d', $good_friday), // Långfredag
                'EASTER_EVE' => date('Y-m-d', $easter_sunday - (1 * $day_seconds)), // Påskafton
                'EASTER_DAY' => date('Y-m-d', $easter_sunday), // Påskdagen
                'EASTER_MONDAY' => date('Y-m-d', $easter_sunday + (1 * $day_seconds)), // Annandag påsk
                'WALPURGIS_NIGHT ' => $year . '-04-30', //   Valborgsmässoafton
                'MAY_FIRST' => $year . '-05-01', //   Första maj
                'ASCENSION_OF_JESUS' => date('Y-m-d', $easter_sunday + (39 * $day_seconds)), //  Kristi himmelsfärdsdag
                'PENTECOST' => date('Y-m-d', $easter_sunday + (49 * $day_seconds)), // Pingstdagen
                'NATIONAL_DAY_OF_SWEDEN' => $year . '-06-06', // Sveriges nationaldag
                'MIDSUMMER_EVE' => date('Y-m-d', $midsummer_day - (1 * $day_seconds)), // Midsommarafton
                'MIDSUMMER_DAY' => date('Y-m-d', $midsummer_day), // Midsommardagen
                'ALL_SAINTS_DAY' => date('Y-m-d', $all_saints_day), // Alla helgons dag
                'CHRISTMAS_EVE' => $year . '-12-24', // Julafton
                'CHRISTMAS_DAY' => $year . '-12-25', // Juldagen
                'CHRISTMAS_DAY_DAY' => $year . '-12-26', // Annandag jul
                'NEW_YEARS_EVE' => $year . '-12-31' // Nyårsafton
            );
        }
    }

