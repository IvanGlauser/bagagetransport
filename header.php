<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<title><?php wp_title(''); ?></title>
	<meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" media="Screen" href="<?php bloginfo('stylesheet_url'); ?>" />
	<link rel="stylesheet" type="text/css" media="Screen" href="<?php bloginfo('template_directory'); ?>/normalize.css" />
	<!--link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet"-->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/js/jquery-tooltipster/css/tooltipster.bundle.min.css" />
	<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOqken5ZZBp6n8GN-Xj2np4dYJ1MeLFS8&libraries=places"></script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
	<header>
		<?php
	    $gallery_images = get_field('image_gallery');
		if($gallery_images){ ?>
			<div id="slider" class="flexslider">
				<?php /*<div class="shadow"></div>*/ ?>
				<ul class="slides">'
			<?php 
				foreach($gallery_images as $image){ ?>
					<li class="slide">
						<div style="background-image: url('<?php echo $image['sizes']['widescreen']; ?>')" />
					</li><?php
				} ?>
				</ul>
			</div><?php
		} ?>

		<div class="logo-container">
			<h1>
				<a href="<?php echo home_url(); ?>">
					<img src="<?php bloginfo('template_directory'); ?>/images/bagagetransport-logo-white@2x.png" alt="bagagetransport" width="214" height="52" />
				</a>
			</h1>
		</div>

		<div class="languages">
			<?php qtranxf_generateLanguageSelectCode('image'); ?>
		</div>
		<div class="center">
			<h1><?php echo esc_html__( 'Transport av bagage', 'bagagetransport' ); ?> <span><?php echo esc_html__( 'upp till 75 km inom Sverige', 'bagagetransport' ); ?></span></h1>
			<div class="cta-button book-now"><a href="#itineraries" class="button nobreak"><?php echo esc_html__( 'Boka transport', 'bagagetransport' ); ?></a></div>
			
		</div>

		<button class="button help-open" href=""><?php echo esc_html__( 'Hjälp?', 'bagagetransport' ); ?></button>
	</header>