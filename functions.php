<?php
	require_once('includes/custom-woocommerce.php');
	require_once('includes/holidays.php');
?>
<?php
	add_theme_support('post-thumbnails', array('page', 'post'));
	add_image_size('widescreen', 1366, 768, true);
?>
<?php
function current_page_url() {
	$pageURL = 'http';
	if( isset($_SERVER["HTTPS"]) ) {
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}
?>
<?php
	// Localize theme
	load_theme_textdomain( 'bagagetransport', '/languages' );

	function bagagetransport_setup(){
		$domain = 'bagagetransport';
		// wp-content/languages/theme-name/de_DE.mo
		load_theme_textdomain($domain, trailingslashit(WP_LANG_DIR).$domain);
		// wp-content/themes/child-theme-name/languages/de_DE.mo
		load_theme_textdomain($domain, get_stylesheet_directory().'/languages');
		// wp-content/themes/theme-name/languages/de_DE.mo
		load_theme_textdomain($domain, get_template_directory().'/languages');

	}
	add_action('after_setup_theme', 'bagagetransport_setup');
?>
<?php
	// Add custom menu
	function bagagetransport_add_menus() {
		register_nav_menus(
			array(
				'footer_nav' => 'Footer Menu',
			)
		);
	}
	add_action('init', 'bagagetransport_add_menus');
	
	function bagagetransport_menu() {
		wp_nav_menu('menu=footer_nav');
	}
	
	function home_page_menu_args( $args ) {
		$args['show_home'] = true;
		return $args;
	}
	
	add_filter('wp_page_menu_args', 'home_page_menu_args');





	// WooCommerce - Add dummy item to cart on visit if cart is empty
	add_action('template_redirect', 'add_dummy_product_to_cart');
	function add_dummy_product_to_cart() {
		$product_id = 33; //8
		if (sizeof(WC()->cart->get_cart()) == 0) {
			// WC()->cart->add_to_cart($product_id);
			$day_after_tomorrow = date('Y-m-d', strtotime(date('Y-m-d')."+2 days"));
			$start_date = get_next_valid_weekday($day_after_tomorrow);

			add_product_to_cart(10, 1, $start_date, '', '', 1, 0);
		}
	}

    add_action( 'wp_ajax_nopriv_post_love_add_love', 'post_love_add_love' );
	add_action( 'wp_ajax_post_love_add_love', 'post_love_add_love' );
	function post_love_add_love() {
		$productId = $_POST['product_id'];
		//$productVariantId = $_POST['product_variant_id'];
		$itineraryNr = $_POST['product_itinerary_nr'];
		$itineraryDate = $_POST['product_itinerary_date'];
		$itineraryStart = $_POST['product_itinerary_start'];
		$itineraryEnd = $_POST['product_itinerary_end'];
		$nrOfItems = $_POST['product_nr_of_items'];
		$itineraryDistance = $_POST['product_itinerary_distance'];

		$returnValue = add_product_to_cart($productId, $itineraryNr, $itineraryDate, $itineraryStart, $itineraryEnd, $nrOfItems, $itineraryDistance);

		echo $returnValue;

		die();
	}

	// WooCommerce - Add item to cart on visit
	//add_action('template_redirect', 'add_product_to_cart');
	function add_product_to_cart($product_id, $itinerary_nr, $itinerary_date, $itinerary_start, $itinerary_end, $nr_of_items, $itinerary_distance) {
		$cart_item_data = array();
		//$variation = array();

		// Spara värden till ordern i varukorgen
		$cart_item_data['Datum'] = $itinerary_date;
		$cart_item_data['Start'] = $itinerary_start;
		$cart_item_data['Mål'] = $itinerary_end;
		$cart_item_data['Kollin'] = $nr_of_items;
		$cart_item_data['Avstånd'] = $itinerary_distance + ' km';
		
		/*if ($product_variant_id == 34){
			$variation['Zon'] = '0 km';
		}
		if ($product_variant_id == 15){
			$variation['Zon'] = '1-25 km';
		}
		if ($product_variant_id == 17){
			$variation['Zon'] = '26-50 km';
		}
		if ($product_variant_id == 16){
			$variation['Zon'] = '51-75 km';
		}*/

		//$cart_item_data = array();
		$cart_item_data['itinerary_nr'] = $itinerary_nr;

		$cart_item_key = null;
		$current_product_cart_item_key = get_product_key_in_cart_by_itinerary_nr($itinerary_nr);
		if(!$current_product_cart_item_key && $product_id != 0){
			// Add product to cart
			$cart_item_key = WC()->cart->add_to_cart($product_id, 1, null, null, $cart_item_data);
		} else {
			// update existing product by removing existing and adding a new
			WC()->cart->remove_cart_item($current_product_cart_item_key);
			$cart_item_key = WC()->cart->add_to_cart($product_id, 1, null, null, $cart_item_data);
		}

		$new_cart_item = WC()->cart->get_cart_item($cart_item_key);
		$new_product_price = get_product_price($new_cart_item);
		$new_cart_item['data']->set_price($new_product_price);
		//print_r($new_cart_item);

		/*$product_variant = new WC_Product_Variation($product_variant_id);
		$product_variant_price = wc_get_price_including_tax($product_variant);//get_price_including_tax();
		$price = $product_variant_price;*/

		/*$price = get_price_by_zone($itinerary_distance);

		$price_extra_by_distance = get_price_extra_by_distance($itinerary_distance);
		$price += $price_extra_by_distance;

		$price = price_modify_by_date($itinerary_date, $price);*/


		$cart_total = WC()->cart->cart_contents_total;
		$tax_total = WC()->cart->tax_total;

		return '{ "itinerary": "'.$itinerary_nr.'", "price": "'.$new_product_price.'", "cart_total_price": "'.($cart_total + $tax_total).'", "cart_total_tax": "'.$tax_total.'" }';
	}

	function get_product_key_in_cart_by_itinerary_nr($itinerary_nr) {
		foreach(WC()->cart->get_cart() as $cart_item_key => $cart_item) {
			if($cart_item['itinerary_nr'] == $itinerary_nr){
				return $cart_item_key;
			}
		}

		return null;
	}

	function get_price_by_zone($distance) {
		if($distance > 51) {
			return 570;
		}
		if($distance > 25) {
			return 375;
		}
		if($distance > 0) {
			return 250;
		}
		else {
			return 0;
		}
	}

	function get_price_extra_by_distance($distance) {
        if($distance > 75){
        	return ($distance - 75) * 15;
        }

        return 0;
	}

	function price_modify_by_date($date, $price) {
		if(is_weekend_or_holiday($date)){
			return ceil($price *= 1.3);
		}

        return $price;
	}

	add_action( 'wp_ajax_nopriv_is_swedish_holiday', 'is_swedish_holiday' );
	add_action( 'wp_ajax_is_swedish_holiday', 'is_swedish_holiday' );
	function is_swedish_holiday() {
		$date = $_POST['date'];
		echo is_weekend_or_holiday($date);

		wp_die();
	}

	function is_weekend_or_holiday($date){
		$weekend = (date('N', strtotime($date)) >= 6);
		if($weekend){
			return $date;
		}

		return Holidays::isHoliday($date);
	}

	function get_next_valid_weekday($date) {
		if(empty($date)){
			$date = date('Y-m-d');
		}
		if(!is_weekend_or_holiday($date)){
			return $date;
		}

		$next_day = date('Y-m-d', strtotime($date."+1 days"));
		return get_next_valid_weekday($next_day);
	}


	add_action( 'wp_ajax_nopriv_bagagetransport_is_ready_for_checkout', 'bagagetransport_is_ready_for_checkout' );
	add_action( 'wp_ajax_bagagetransport_is_ready_for_checkout', 'bagagetransport_is_ready_for_checkout' );
	function bagagetransport_is_ready_for_checkout() {
		global $woocommerce;

		$cart_items = WC()->cart->get_cart();

		foreach($cart_items as $cart_item_key => $cart_item) {
			//$itinerary_nr = $cart_item['itinerary_nr'];
			//$price = $cart_item['line_total'] + $cart_item['line_tax'];

			//$variation = $cart_item['variation'];
			$distance = $cart_item['Avstånd'];
			$price = $cart_item['line_total'] + $cart_item['line_tax'];
			//$zone = $variation['Zon'];

			if($distance == '0' && $price == 0){
				echo 'false';
				die();
			}
		}

		echo 'true';
		die();
	}

	function show_cart_products(){
		$nr_of_items_in_cart = WC()->cart->get_cart_contents_count();
		if ($nr_of_items_in_cart == 0) {
        	// Cart is empty - show default input fields
        	// TODO: Also show default input fields if only dummy product is present
			showItinerary(1, date('Y-m-d'), '', '', 1, '', '');
		} else {
			// Sort cart items
			$cartItems = WC()->cart->get_cart();
			$sortedCartItems = array();
			foreach ($cartItems as $key => $cart_item) {
			    $sortedCartItems[$cart_item['itinerary_nr']] = $cart_item;
			}
			ksort($sortedCartItems);

			foreach($sortedCartItems as $cart_item_key => $cart_item) {
				$itinerary_nr = $cart_item['itinerary_nr'];
				$price = $cart_item['line_total'] + $cart_item['line_tax'];

				$date = $cart_item['Datum'];
				$start_location = $cart_item['Start'];
				$end_location = $cart_item['Mål'];
				$nr_of_items = $cart_item['Kollin'];
				$distance = $cart_item['Avstånd'];
				//$zone = $cart_item['Zon'];

				/*$variation = $cart_item['variation'];
				$date = $variation['Datum'];
				$start_location = $variation['Start'];
				$end_location = $variation['Mål'];
				$nr_of_items = $variation['Kollin'];
				$distance = $variation['Avstånd'];
				$zone = $variation['Zon'];*/

				$tomorrow = date('Y-m-d', strtotime("+2 days"));
				if ($date <= $tomorrow) {
					$date = $tomorrow;
				}

				showItinerary($itinerary_nr, $date, $start_location, $end_location, $nr_of_items, $distance, $price);
			}
		}
	}

	function showItinerary($itinerary_nr, $date, $start_location, $end_location, $nr_of_items, $distance, $price){
		echo '
		<div class="itinerary" data-itinerary-nr="'.$itinerary_nr.'">
			<div class="row">
				<div class="group">
					<h2>'.esc_html__( 'Sträcka', 'bagagetransport' ).' <span>'.$itinerary_nr .'</span></h2>
					<button class="button removeItinerary tooltip" title="Ta bort denna sträcka">-</button>
					<div class="row">
						<div>
							<label>'.esc_html__( 'Datum', 'bagagetransport' ).'</label><input class="itineraryDate tooltip" name="date" type="date" min="'.date('Y-m-d', strtotime(date('Y-m-d')."+2 days")).'" value="'.$date.'" title="Kontakta oss om du vill göra en bokning med kortare varsel än 2 dagar">
						</div>
						<div>
							<label>'.esc_html__( 'Antal kollin', 'bagagetransport' ).'</label><input class="nrOfItems tooltip" name="nr_of_items" type="number" size="50" value="'.$nr_of_items.'" title="I priset ingår upp till 50 kg oavsett antal väskor">
						</div>
					</div>
					<!--div class="row">
						<div>
							<select multiple size="6"> 
								<option value="bag">Väska</option>
								<option value="golfbag">Golfbag</option>
								<option value="bicycle">Cykel</option>
							</select>
						</div>
						<div>
							<label>Totalvikt</label>
							<select>
								<option value="value1">0 - 20 kg</option> 
								<option value="value2">21 - 50 kg</option> 
								<option value="value2">51 - 100 kg</option> 
								<option value="value2">101 - 150 kg</option> 
								<option value="value2">151 - 200 kg</option> 
								<option value="value2">201 - 300 kg</option> 
							</select>
						</div>
					</div-->
					<label>'.esc_html__( 'Från', 'bagagetransport' ).'</label><input class="fromDestination searchTextField tooltip" name="from_destination" type="text" size="50" value="'.$start_location.'" title="Var god ange en så exakt upphämtningadress som möjligt. Upphämtning sker efter klockan 10:00.">
					<label>'.esc_html__( 'Till', 'bagagetransport' ).'</label><input class="toDestination searchTextField tooltip" name="to_destination" type="text" size="50" value="'.$end_location.'" title="Var god ange en så exakt avlämningsadress som möjligt. Ditt bagage levereras senast klockan 16:00.">
					<div>
						<label><input type="checkbox" name="max50kg" value="50" checked>'.esc_html__( 'Totala vikten bagage är under 50 kg', 'bagagetransport' ).'</label>
					</div>
					<div class="info">
						<div class="itinerary-distance">'.esc_html__( 'Avstånd', 'bagagetransport' ).': <span class="distance">'.$distance.'</span> km</div>
						<div class="itinerary-price">'.esc_html__( 'Pris', 'bagagetransport' ).': <span class="price">'.$price.'</span> kr</div>
					</div>
				</div>
				<div>
					<div id="map" class="map"></div>
				</div>
			</div>
			<div class="notes"></div>
		</div>';
	}


	// Remove Order Notes from checkout field in Woocommerce
	add_filter('woocommerce_checkout_fields', 'alter_woocommerce_checkout_fields');
	function alter_woocommerce_checkout_fields($fields) {
		unset($fields['order']['order_comments']);
		return $fields;
	}


	// Clear cart function
	add_action('wp_ajax_nopriv_woocommerce_clear_cart', 'woocommerce_clear_cart');
	add_action('wp_ajax_woocommerce_clear_cart', 'woocommerce_clear_cart');
	function woocommerce_clear_cart() {
		WC()->cart->empty_cart();
		die();
	}

	// Remove itinerary function
	add_action('wp_ajax_nopriv_woocommerce_remove_itinerary', 'woocommerce_remove_itinerary');
	add_action('wp_ajax_woocommerce_remove_itinerary', 'woocommerce_remove_itinerary');
	function woocommerce_remove_itinerary() {
		$itineraryNr = $_POST['product_itinerary_nr'];
		
		$product_cart_item_key = get_product_key_in_cart_by_itinerary_nr($itineraryNr);
		if($product_cart_item_key){
            WC()->cart->remove_cart_item($product_cart_item_key);
		}

		die();
	}
?>
<?
/*
	// Add the personummer to the checkout
	add_action('woocommerce_after_order_notes', 'my_custom_checkout_field');
	function my_custom_checkout_field( $checkout ) {
		
		echo '<div id="my_custom_checkout_field">';
					
		/**
		 * Output the field. This is for 1.4.
		 **/
/*		woocommerce_form_field( 'personnummer_field', array( 
			'type' 			=> 'text', 
			'class' 		=> array('my-field-class orm-row-wide'), 
			'label' 		=> __('Personnummer'), 
			'placeholder' 	=> __('YYMMDD-XXXX'),
			'required'      => true,
			), $checkout->get_value( 'personnummer_field' ));
		
		echo '</div>';
	}

	/**
	 * Process the checkout
	 **/
/*	add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');
	function my_custom_checkout_field_process() {
		global $woocommerce;
		
		// Check if set, if its not set add an error. This one is only requite for companies
		if ($_POST['billing_company']){
			if (!$_POST['personnummer_field']) {
				$woocommerce->add_error( __('Please enter your XXX.') );
			}
		}
	}

	/**
	 * Update the user meta with field value
	 **/
/*	add_action('woocommerce_checkout_update_user_meta', 'my_custom_checkout_field_update_user_meta');
	function my_custom_checkout_field_update_user_meta( $user_id ) {
		if ($user_id && $_POST['personnummer_field']) {
			update_user_meta( $user_id, 'personnummer_field', esc_attr($_POST['personnummer_field']) );
		}
	}

	/**
	 * Update the order meta with field value
	 **/
/*	add_action('woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta');
	function my_custom_checkout_field_update_order_meta( $order_id ) {
		if ($_POST['personnummer_field']){
			update_post_meta( $order_id, 'Personnummer', esc_attr($_POST['personnummer_field']));
		}
	}

	/**
	 * Add the field to order emails
	 **/
/*	add_filter('woocommerce_email_order_meta_keys', 'my_custom_checkout_field_order_meta_keys');
	function my_custom_checkout_field_order_meta_keys( $keys ) {
		$keys[] = 'Personnummer';
		return $keys;
	}*/
?>
<?php
	// Include jquery
	if (!is_admin()) add_action("wp_enqueue_scripts", "bagagetransport_jquery_enqueue", 11);
	function bagagetransport_jquery_enqueue() {
		wp_deregister_script('jquery');
		wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js", false, null);
		wp_enqueue_script('jquery');

		// TODO: Load only on front-page.
	    wp_deregister_script('flexslider');
	    wp_register_script('flexslider', get_stylesheet_directory_uri().'/js/flexslider/jquery.flexslider-min.js', array('jquery'), '', true);
	    wp_enqueue_script('flexslider');

	    wp_deregister_script('tooltipster');
	    wp_register_script('tooltipster', get_stylesheet_directory_uri().'/js/jquery-tooltipster/js/tooltipster.bundle.min.js', array('jquery'), '', true);
	    wp_enqueue_script('tooltipster');

		wp_enqueue_script('moment', get_stylesheet_directory_uri().'/js/moment/moment.min.js', '', '', true);
		wp_enqueue_script('moment');

		wp_deregister_script('bagagetransport');
		wp_register_script('bagagetransport', get_template_directory_uri() . "/js/theme.js", false, null);
		wp_enqueue_script('bagagetransport');

	   	wp_localize_script('bagagetransport', 'postlove', array(
			'ajax_url' => admin_url('admin-ajax.php')
		));
	}
?>
<?php
	// Custom login style
	add_action('login_head', 'custom_login');
	
	function custom_login() { 
		echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('template_directory').'/custom-login/custom-login.css" />'; 
	}
	
	// Custom login logo link
	function wpc_url_login(){
		return "https://bagagetransport.se/";
	}
	add_filter('login_headerurl', 'wpc_url_login');
?>
<?php
	// Custom dashboard help
	add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

	function my_custom_dashboard_widgets() {
	   global $wp_meta_boxes;

	   unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	   unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	   unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);

	   wp_add_dashboard_widget('custom_help_widget', 'Help and Support', 'custom_dashboard_help');
	}
	
	function custom_dashboard_help() {
	   echo '<p>Welcome to the bagagetransport.com! Need help? Contact <a href="http://chamigo.se">Chamigo</a>.</p>';
	}
?>
<?php
	// Custom admin footer
	add_filter('admin_footer_text', 'remove_footer_admin');
	
	function remove_footer_admin () {
    echo 'Powered by <a href="http://www.wordpress.org">WordPress</a> | <a href="http://codex.wordpress.org/">Documentation</a> | <a href="http://wordpress.org/support/forum/4">Feedback</a> | bagagetransport theme developed by <a href="http://chamigo.se">Chamigo</a></p>';
    }
?>