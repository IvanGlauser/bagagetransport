jQuery(document).ready(function($) {

    // Home-page
    $slider = $('#slider.flexslider');
    if($slider.length){
        $('#slider.flexslider').flexslider({
            animation: 'fade',
            slideshowSpeed: 9000,
            animationSpeed: 900,
            pauseOnAction: false,
            pauseOnHover: false,
            controlNav: false,
            directionNav: false, 
            initDelay: 0,
            randomize: true,
            controlsContainer: ".flexslider"
        });
    }

    $('.cta-button.book-now').on('click', 'a', function(event){
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 500);
    });

    $('.tooltip').tooltipster({
        theme: ['tooltipster-shadow', 'tooltipster-shadow-customized']
    });
    
    // Toggle private/company customer
    /*$('#billing_customer_email_field').hide();
    $('.customer-select').on('click', 'button.button:not(.selected)',function (e) {
        if($(this).hasClass('for-business')){
            //console.log('Activate business logic');
            $('#billing_customer_email_field').show();
        } else {
            $('#billing_customer_email_field').hide();
        }

    	$('.customer-select button.selected').removeClass('selected');
        $(this).addClass('selected');
    });*/


    // Help pages & FAQ
    //$('#help .help-faq').addClass('hidden');
    
    $('button.help-open').on('click',function (e) {
        $('#help').removeClass('hidden');
    });
    $('#help .help-close').on('click',function (e) {
        $(this).closest('#help').addClass('hidden');
    });

    /*$('.help-sections button').on('click',function (e) {
        $('.help-sections button.selected').removeClass('selected');
        $(this).addClass('selected');

        $('#help section').each(function () {
            $(this).addClass('hidden');
        });

        var showSection = $(this).data('section');
        $('#help').find('.' + showSection).removeClass('hidden');
    });*/

    // Help FAQ
    $('.help-faq-questions li .answer').each(function () {
        $(this).addClass('hidden');
    });
    $('.help-faq-questions li').on('click',function (e) {
        $(this).find('.answer').toggleClass('hidden');
    });


    // On itinerary price change
    $('#itineraries').on('DOMSubtreeModified', '.itinerary .info span.price', function() {
        $(this).fadeTo(100, 0.1).fadeTo(200, 1.0);
    });


    updateCheckoutVisibility();

    // Initialize itineraries
    $('.itinerary').each(function() {
        initializeItinerary(this);
    });

    // Add itinerary
    $('button#addItinierary').on('click',function (e) {
        e.preventDefault();
        $itinerary = $('.itinerary:last');
        $newItinerary = $itinerary.clone();

        // Get previous end
        var previousEnd = $itinerary.find('input.toDestination').val();
        var previousDate = new Date($itinerary.find('input.itineraryDate').val());
        var previousItineraryNr = $itinerary.data('itinerary-nr');
        var newItineraryNumber = previousItineraryNr + 1;
        var newDate = new Date(previousDate.setDate(previousDate.getDate() + 1));
        var day = ("0" + newDate.getDate()).slice(-2);
        var month = ("0" + (newDate.getMonth() + 1)).slice(-2);
        var newDateInputValue = newDate.getFullYear()+"-"+(month)+"-"+(day);

        // Set previous end as new start destination as default
        $newItinerary.attr('data-itinerary-nr', newItineraryNumber);
        $newItinerary.find('input.fromDestination').val(previousEnd);        
        $newItinerary.find('input.toDestination').val('');
        $newItinerary.find('input.itineraryDate').val(newDateInputValue);

        // Update itinerary number, price and distance
        $newItinerary.find('h2 span').html(newItineraryNumber);
        $newItinerary.find('.price').html('0');
        $newItinerary.find('.distance').html('0');
        //$newItinerary.find('.alert').hide();
        //$newItinerary.find('.warning').hide();

        $newItinerary.insertAfter($itinerary);

        // Initi new itinerary
        initializeItinerary($newItinerary[0]);
    });

    // Remove itinerary
    $('#itineraries').on('click', 'button.removeItinerary', function (e) {
        e.preventDefault();
        $itinerary = $(this).closest('.itinerary');
        var itineraryNr = $itinerary.data('itinerary-nr');
        removeItineraryFromCart(itineraryNr);
    });

    // Clear cart
    /*$('button#clearCart').on('click', function (e) {
        e.preventDefault();
        clearCart();
    });*/

    function initializeItinerary(itinerary){
        itinerary.nr = $(itinerary).data('itinerary-nr');

        // Init map
        initializeMap(itinerary);

        // Init autocomplete
        initializeAutocomplete(itinerary);

        // Calculate routes
        calculateRoute(itinerary, false);

        // Set listeners for changes
        initializeInputListeners(itinerary);
    }

    // TODO: move map style to other file
    function initializeMap(itinerary) {
        // Start in Sweden
        var startPosition = new google.maps.LatLng(58.688359, 14.567871);
        var mapOptions = {
            zoom: 7,
            center: startPosition,
            mapTypeControl: false,
            streetViewControl: false,
            styles: [
                {
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#f5f5f5"
                      }
                    ]
                  },
                  {
                    "elementType": "labels.icon",
                    "stylers": [
                      {
                        "visibility": "off"
                      }
                    ]
                  },
                  {
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#616161"
                      }
                    ]
                  },
                  {
                    "elementType": "labels.text.stroke",
                    "stylers": [
                      {
                        "color": "#f5f5f5"
                      }
                    ]
                  },
                  {
                    "featureType": "administrative.land_parcel",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#bdbdbd"
                      }
                    ]
                  },
                  {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#eeeeee"
                      }
                    ]
                  },
                  {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#757575"
                      }
                    ]
                  },
                  {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#e5e5e5"
                      }
                    ]
                  },
                  {
                    "featureType": "poi.park",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#9e9e9e"
                      }
                    ]
                  },
                  {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#ffffff"
                      }
                    ]
                  },
                  {
                    "featureType": "road.arterial",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#757575"
                      }
                    ]
                  },
                  {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#dadada"
                      }
                    ]
                  },
                  {
                    "featureType": "road.highway",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#616161"
                      }
                    ]
                  },
                  {
                    "featureType": "road.local",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#9e9e9e"
                      }
                    ]
                  },
                  {
                    "featureType": "transit.line",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#e5e5e5"
                      }
                    ]
                  },
                  {
                    "featureType": "transit.station",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#eeeeee"
                      }
                    ]
                  },
                  {
                    "featureType": "water",
                    "stylers": [
                      {
                        "weight": 1
                      }
                    ]
                  },
                  {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                      {
                        "color": "#c9c9c9"
                      }
                    ]
                  },
                  {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                      {
                        "color": "#9e9e9e"
                      }
                    ]
                }
            ]
        };

        var mapElement = $(itinerary).find('.map').first()[0];
        var map = new google.maps.Map(mapElement, mapOptions);
        var directionsDisplay = new google.maps.DirectionsRenderer();
        directionsDisplay.setMap(map);

        itinerary.map = map;
        itinerary.directionsDisplay = directionsDisplay;
    }

    function initializeAutocomplete(itinerary) {
        var options = {
            //types: ['geocode'],
            componentRestrictions: {country: "se"}
        };

        $(itinerary).find('.searchTextField').each(function() {
            var autocomplete = new google.maps.places.Autocomplete($(this).get(0), options);
            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                calculateRoute(itinerary, true);
            });
        });
    }
    
    function calculateRoute(itinerary, shouldUpdateCart) {
        $itinerary = $(itinerary);
        var date = $itinerary.find('input.itineraryDate').val();

        // Validate date
        var today = moment();
        var tomorrow = today.add(1, 'days');
        //console.log(date);
        if (moment(date) < tomorrow){
            //console.log('not today or tomorrow');
            $itinerary.find('input[type="date"]').addClass('alert');
            showWarningMessage($itinerary, '<strong>Tjänsten kan enbart beställas med minst 2 dagars framförhållning.</strong>', 'two_days_ahead', true);

            // Reset this itinerary
            $itinerary.find('.price').html('-');
            //updateCart(0, 0, itinerary.nr, date, start, end, nrOfItems, distance);
            return;
        } else {
            $itinerary.find('input[type="date"]').removeClass('alert');
            removeWarningMessage($itinerary, 'two_days_ahead');
        }

        checkSwedishHoliday($itinerary, date);

        if ($itinerary.find('.price').html() < 2){
            $itinerary.find('.price').html('-');
            $('#cart_total .cart_total').html('-');
        }

        var start = $itinerary.find('input.fromDestination').val();
        var end = $itinerary.find('input.toDestination').val();
        var request = {
            origin: start,
            destination: end,
            travelMode: 'DRIVING'
        };

        /*if(!hasNumbers(start) || !hasNumbers(end)) {
            if(!hasNumbers(start)) {
                showWarningMessage($itinerary, 'Var god ange även gatunummer för FRÅN-adressen.');
            }
            else if(!hasNumbers(end)) {
                showWarningMessage($itinerary, 'Var god ange även gatunummer för TILL-adressen.');
            }
        } else {
            $itinerary.find('.warning').hide();
        }*/

        var directionsService = new google.maps.DirectionsService();
        directionsService.route(request, function(result, status) {
            if (status == 'OK') {
                itinerary.directionsDisplay.setDirections(result);
                var distance = Math.round(result.routes[0].legs[0].distance.value / 1000);
                
                $itinerary = $(itinerary);
                if(distance >= 75) {
                    showWarningMessage($itinerary, 'Då avståndet är <strong>över 75 km</strong> har ett tillägg per km lagts på priset. För längre sträckor, vänligen kontakta kundtjänst <a href="mailto:kundservice@bagagetransport.se">kundservice@bagagetransport.se</a>.', 'long_distance');
                    $itinerary.find('.itinerary-distance').addClass('highlight');
                } else {
                    removeWarningMessage($itinerary, 'long_distance');
                    $itinerary.find('.itinerary-distance').removeClass('highlight');
                }

                if (shouldUpdateCart) {
                    $itinerary.find('.distance').html(distance);
                    var nrOfItems = $itinerary.find('input.nrOfItems').val();
                    
                    // Validate distance
                    if(distance < 26) {
                        updateCart(10, 15, itinerary.nr, date, start, end, nrOfItems, distance);
                        return;
                    }
                    if(distance < 51) {
                        updateCart(10, 17, itinerary.nr, date, start, end, nrOfItems, distance);
                        return;
                    }
                    else { //if(distance < 76) {
                        updateCart(10, 16, itinerary.nr, date, start, end, nrOfItems, distance);
                        return;
                    }
                    /*} else {
                        // Reset this itinerary
                        $itinerary.find('.price').html('-');
                        updateCart(0, 34, itinerary.nr, date, start, end, nrOfItems, distance);
                    }*/
                }
            }
        });
    }

    function checkSwedishHoliday($itinerary, date){
        $.ajax({
            url: postlove.ajax_url,
            type: 'post',
            data: {
                action: 'is_swedish_holiday',
                date: date
            },
            success: function(response) {
                if(response) {
                    showWarningMessage($itinerary, '<strong>Helgdag/röd dag.</strong> Ett tillägg har lagts på priset.', 'weekend_or_holiday');
                    $itinerary.find('input[type="date"]').addClass('highlight');
                } else {
                    removeWarningMessage($itinerary, 'weekend_or_holiday');
                    $itinerary.find('input[type="date"]').removeClass('highlight');
                }
            }
        });
    }

    function hasNumbers(t) {
        var regex = /\d/g;
        return regex.test(t);
    }    

    function initializeInputListeners(itinerary) {
        $(itinerary).find("input.nrOfItems, input.itineraryDate").bind('keyup change', function () {
            calculateRoute(itinerary, true);
        });

        $(itinerary).find('input[name="max50kg"]').change(function() {
            calculateRoute(itinerary, true);

            if($(this).is(':checked')){
                removeWarningMessage($itinerary, 'max_weight');
                calculateRoute(itinerary, true);
            }
            else {
                showWarningMessage($itinerary, 'Var god kontakta kundtjänst för <strong>totalvikt över 50 kg</strong>.', 'max_weight');
            }
        });
    }

    // WooCommerce update cart ajax function
    function updateCart(productId, productVariantId, productItineraryNr, date, start, end, nrOfItems, distance) {
        $.ajax({
            url: postlove.ajax_url,
            type: 'post',
            data: {
                action: 'post_love_add_love',
                product_id: productId,
                product_variant_id: productVariantId,
                product_itinerary_nr: productItineraryNr,
                product_itinerary_date: date,
                product_itinerary_start: start,
                product_itinerary_end: end,
                product_nr_of_items: nrOfItems,
                product_itinerary_distance: distance
            },
            success: function(response) {
                var responseValues = jQuery.parseJSON(response);
                $itinerary = $('.itineraries').find("[data-itinerary-nr='" + responseValues.itinerary + "']");
                if(!responseValues.price || responseValues.price < 10) { // Hack - fix this
                    responseValues.price = '-';
                }
                var current_price = $($itinerary).find('.price').html();
                if(current_price != responseValues.price) {
                    $($itinerary).find('.price').html(responseValues.price);
                }

                var cart_total_price = responseValues.cart_total_price;
                var cart_total_tax = responseValues.cart_total_tax;

                /*var totalcartPrice = 0;
                $('.itinerary .price').each(function (){
                    totalcartPrice = totalcartPrice + parseInt($(this).html());
                    if(!totalcartPrice || totalcartPrice < 10) { // Hack - fix this
                        totalcartPrice = '-';
                    }
                });*/

                var current_total_price = $($itinerary).find('#cart_total .cart_total').html();
                if(current_total_price != cart_total_price) {
                    $("#cart_total .cart_total").html(cart_total_price);
                    $("#cart_total .tax_total .amount").html(cart_total_tax);
                }

                updateCheckoutVisibility();
            }
        });
    }

    // WooCommerce clear cart ajax function
    function clearCart() {
        $.ajax({
            url: postlove.ajax_url,
            type: 'post',
            data: {
                action: 'woocommerce_clear_cart'
            },
            success: function(response) {
                $('.cart_total, .order-total .woocommerce-Price-amount').html('0');
                location.reload();
            }
        });
    }

    // WooCommerce clear cart ajax function
    function removeItineraryFromCart(productItineraryNr) {
        $.ajax({
            url: postlove.ajax_url,
            type: 'post',
            data: {
                action: 'woocommerce_remove_itinerary',
                product_itinerary_nr: productItineraryNr
            },
            success: function(response) {
                location.reload();
            }
        });
    }

    // Show/hide checkout
    function updateCheckoutVisibility(){
        $.ajax({
            url: postlove.ajax_url,
            type: 'get',
            data: {
                action: 'bagagetransport_is_ready_for_checkout'
            },
            success: function(response) {
                if(response === 'true'){
                    //$('#customer-select button').prop("disabled", false);
                    $('#checkout').show();
                } else {
                    //$('#customer-select button').prop("disabled", true);
                    $('#checkout').hide();
                }
            },
            error: function(response) {
                $('#checkout').hide();
            },
        });
    }

    // TODO: add support for multiple alert messages using ids
    /*function showAlertMessage(itinerary, message){
        $(itinerary).find('.alert').html(message).show();
        //$(itinerary).find('.alert').html('<strong>Avståndet är för långt.</strong> Var god kontakta kundtjänst.').show();
    }*/

    function showWarningMessage(itinerary, message, warning_id, is_alert){
        var alertStyle = '';
        if (typeof is_alert !== 'undefined') {
            alertStyle = ' alert';
        }
        $warning = $(itinerary).find('.notes .warning.' + warning_id);
        if(!$warning.length) {
            var warning_message = '<div class="warning ' + warning_id + alertStyle + '">' + message + '</div>';
            $(itinerary).find('.notes').prepend(warning_message);
        } else {
            $warning.html(message);
        }

        /*var warning_message = '<div class="warning">' + message + '</div>';
        if(warning_id){
            $(itinerary).find('.notes .warning.' + warning_id).html(warning_message).show();
        } else {
            $(itinerary).find('.notes').html(warning_message).show();
        }*/
        //$(itinerary).find('.alert').html('<strong>Avståndet är för långt.</strong> Var god kontakta kundtjänst.').show();
    }

    function removeWarningMessage(itinerary, warning_id) {
        $(itinerary).find('.notes .warning.' + warning_id).remove();
    }

    // Validate signup form on keyup and submit
    /*$(".itinerary").validate({
        rules: {
            date: {
                required: true,
                date: true
            },
        },
        messages: {
            date: "Var god ange datum på formatet åååå-mm-dd."
        }
    });*/
});