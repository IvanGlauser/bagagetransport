var markers = [{
    "name": "Ivalo",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/ivalo",
    "desc": "<p>Lentokent\u00e4ntie 290<\/p>\r\n<p>99800 Ivalo<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6019",
        "street": "Lentokent\u00e4ntie",
        "nr": "",
        "city": "",
        "country": "Finland",
        "postalcode": "99800",
        "location": "",
        "lat": "68.6105251",
        "lng": "27.455205999999976"
    }
}, {
    "name": "Jetpak Stockholm",
    "pageLink": "\/se\/vi-finns-har\/jetpak-stockholm",
    "desc": "<p><em>Bes\u00f6ksadress:<\/em><br \/>G\u00e5rdsv\u00e4gen 8<br \/>16970 Solna\u00a0<\/p>\r\n<p><em>Postadress:<\/em><br \/>Box 3009<br \/>16903 Solna<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "13223",
        "street": "G\u00e5rdsv\u00e4gen",
        "nr": "8",
        "city": "Solna",
        "country": "Sverige",
        "postalcode": "16970",
        "location": "",
        "lat": "59.3669051",
        "lng": "18.011921099999995"
    }
}, {
    "name": "Jetpak Bor\u00e5s",
    "pageLink": "\/se\/vi-finns-har\/boras",
    "desc": "<p><span>Gr\u00f6nbogatan 5<\/span><br \/><span>504 68 BOR\u00c5S<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "13997",
        "street": "Gr\u00f6nbogatan",
        "nr": "5",
        "city": "Bor\u00e5s",
        "country": "Sverige",
        "postalcode": "504 68",
        "location": "",
        "lat": "57.7284186",
        "lng": "12.919018499999993"
    }
}, {
    "name": "Jetpak Karlstad",
    "pageLink": "\/se\/vi-finns-har\/karlstad",
    "desc": "<p><span>Gjuterigatan 3<br \/><\/span>Box 5137<br \/>652 21 Karlstad<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "570",
        "street": "Gjuterigatan",
        "nr": "3",
        "city": "Karlstad",
        "country": "Sverige",
        "postalcode": "65221",
        "location": "",
        "lat": "59.38143609999999",
        "lng": "13.533730099999957"
    }
}, {
    "name": "Jetpak Arvidsjaur",
    "pageLink": "\/se\/vi-finns-har\/arvidsjaur",
    "desc": "<p>Arvidsjaur Flygplats<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5451",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Sverige",
        "postalcode": "93391",
        "location": "",
        "lat": "65.590278",
        "lng": "19.281944000000067"
    }
}, {
    "name": "Jetpak Borl\u00e4nge",
    "pageLink": "\/se\/vi-finns-har\/borlange",
    "desc": "<p>Godsv\u00e4gen 17<br \/>784 72 BORL\u00c4NGE<\/p>\r\n<p>Tel \u00a0<span>0243-23 98 55<\/span><br \/><br \/><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "11831",
        "street": "Godsv\u00e4gen",
        "nr": "17",
        "city": "Borl\u00e4nge",
        "country": "Sverige",
        "postalcode": "78472",
        "location": "",
        "lat": "60.48817889999999",
        "lng": "15.478656900000033"
    }
}, {
    "name": "Jetpak G\u00e4llivare",
    "pageLink": "\/se\/vi-finns-har\/gallivare",
    "desc": "<p><span>Metallv\u00e4gen 2<\/span><br \/><span>982 38 G\u00c4LLIVARE<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "759",
        "street": "Metallv\u00e4gen",
        "nr": "2",
        "city": "G\u00e4llivare",
        "country": "Sverige",
        "postalcode": "98238",
        "location": "",
        "lat": "67.1462511",
        "lng": "20.645801699999993"
    }
}, {
    "name": "Jetpak G\u00e4vle",
    "pageLink": "\/se\/vi-finns-har\/gavle",
    "desc": "<p><span>R\u00e4lsgatan 7A\u00a0<\/span><br \/><span>802 91 G\u00c4VLE<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "13025",
        "street": "R\u00e4lsgatan",
        "nr": "7A",
        "city": "G\u00e4vle",
        "country": "Sverige",
        "postalcode": "802 91",
        "location": "",
        "lat": "60.66339909999999",
        "lng": "17.174326699999938"
    }
}, {
    "name": "Jetpak G\u00f6teborg",
    "pageLink": "\/se\/vi-finns-har\/goteborg",
    "desc": "<p>Kryptongatan 14<br \/>431 53 M\u00f6lndal<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "12592",
        "street": "Kryptongatan",
        "nr": "14",
        "city": "M\u00f6lndal",
        "country": "Sverige",
        "postalcode": "431 53",
        "location": "",
        "lat": "57.6390316",
        "lng": "12.011775100000023"
    }
}, {
    "name": "Jetpak Helsingborg",
    "pageLink": "\/se\/vi-finns-har\/helsingborg",
    "desc": "<p><span>Mogatan 2\u00a0<\/span><br \/><span>254 64 HELSINGBORG<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "13213",
        "street": "Mogatan",
        "nr": "2",
        "city": "Helsingborg",
        "country": "Sverige",
        "postalcode": "25464",
        "location": "",
        "lat": "56.07038799999999",
        "lng": "12.75646400000005"
    }
}, {
    "name": "Jetpak Landvetter",
    "pageLink": "\/se\/vi-finns-har\/landvetter",
    "desc": "<p>Box 2091<br \/>438 13 Landvetter<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "869",
        "street": "",
        "nr": "",
        "city": "Landvetter",
        "country": "Sverige",
        "postalcode": "438 80",
        "location": "",
        "lat": "57.668799",
        "lng": "12.292314000000033"
    }
}, {
    "name": "Jetpak Halmstad",
    "pageLink": "\/se\/vi-finns-har\/halmstad",
    "desc": "<p><span>T\u00f6mv\u00e4gen 7\u00a0<\/span><br \/><span>302 62 HALMSTAD<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "875",
        "street": "T\u00f6mv\u00e4gen",
        "nr": "7",
        "city": "Halmstad",
        "country": "Sverige",
        "postalcode": "30262",
        "location": "",
        "lat": "56.64157729999999",
        "lng": "12.932057999999984"
    }
}, {
    "name": "Jetpak Hudiksvall",
    "pageLink": "\/se\/vi-finns-har\/hudiksvall",
    "desc": "<p><span>Hede-Finnflov\u00e4gen 3E\u00a0<\/span><br \/><span>824 92 HUDIKSVALL<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "893",
        "street": "Hede-finnflov\u00e4gen",
        "nr": "3E",
        "city": "Hudiksvall",
        "country": "Sverige",
        "postalcode": "82492",
        "location": "",
        "lat": "61.737235",
        "lng": "17.084003800000005"
    }
}, {
    "name": "Jetpak J\u00f6nk\u00f6ping",
    "pageLink": "\/se\/vi-finns-har\/jonkoping",
    "desc": "<p><span>Skifferv\u00e4gen 6<\/span><br \/><span>553 03 J\u00d6NK\u00d6PING<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "915",
        "street": "Skifferv\u00e4gen",
        "nr": "6",
        "city": "J\u00f6nk\u00f6ping",
        "country": "Sverige",
        "postalcode": "55303",
        "location": "",
        "lat": "57.7658784",
        "lng": "14.171712999999954"
    }
}, {
    "name": "Jetpak Kalmar",
    "pageLink": "\/se\/vi-finns-har\/kalmar",
    "desc": "<p><span>Flottiljv\u00e4gen 14\u00a0<\/span><br \/><span>392 41 KALMAR<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "917",
        "street": "Flottiljv\u00e4gen",
        "nr": "14",
        "city": "Kalmar",
        "country": "Sverige",
        "postalcode": "39241",
        "location": "",
        "lat": "56.6752886",
        "lng": "16.289806099999964"
    }
}, {
    "name": "Jetpak Kiruna",
    "pageLink": "\/se\/vi-finns-har\/kiruna",
    "desc": "<p><span>Flygplatsen\u00a0<\/span><br \/><span>Box 151\u00a0<\/span><br \/><span>981 23 KIRUNA<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "921",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Sverige",
        "postalcode": "",
        "location": "",
        "lat": "67.827199",
        "lng": "20.33679499999994"
    }
}, {
    "name": "Jetpak Kramfors - Sollefte\u00e5",
    "pageLink": "\/se\/vi-finns-har\/solleftea",
    "desc": "<p>Kramfors flygplats<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5440",
        "street": "Gistg\u00e5rds\u00f6n 101",
        "nr": "",
        "city": "",
        "country": "Sverige",
        "postalcode": "87052",
        "location": "",
        "lat": "63.049207",
        "lng": "17.768723000000023"
    }
}, {
    "name": "Jetpak Kristianstad",
    "pageLink": "\/se\/vi-finns-har\/kristianstad",
    "desc": "<p><span>Bromsaregatan 6<\/span><br \/><span>291 59 KRISTIANSTAD<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "932",
        "street": "Bromsaregatan",
        "nr": "6",
        "city": "Kristianstad",
        "country": "Sverige",
        "postalcode": "29159",
        "location": "",
        "lat": "56.02194799999999",
        "lng": "14.152152099999967"
    }
}, {
    "name": "Jetpak Link\u00f6ping",
    "pageLink": "\/se\/vi-finns-har\/linkoping",
    "desc": "<p>Id\u00f6gatan 14<br \/>582 78 LINK\u00d6PING<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "12983",
        "street": "Id\u00f6gatan",
        "nr": "14",
        "city": "Link\u00f6ping",
        "country": "Sweden",
        "postalcode": "582 78",
        "location": "",
        "lat": "58.42239230000001",
        "lng": "15.675813800000014"
    }
}, {
    "name": "Jetpak Lule\u00e5",
    "pageLink": "\/se\/vi-finns-har\/lulea",
    "desc": "<p><span>Rampv\u00e4gen 2\u00a0<\/span><br \/><span>971 03 LULE\u00c5<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "11965",
        "street": "Rampv\u00e4gen",
        "nr": "",
        "city": "",
        "country": "Sverige",
        "postalcode": "",
        "location": "",
        "lat": "65.553583",
        "lng": "22.11550269999998"
    }
}, {
    "name": "Jetpak Lycksele",
    "pageLink": "\/se\/vi-finns-har\/lycksele",
    "desc": "<p>Lycksele flygplats<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5446",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Sverige",
        "postalcode": "92181",
        "location": "",
        "lat": "64.5475",
        "lng": "18.717777999999953"
    }
}, {
    "name": "Jetpak Norrk\u00f6ping",
    "pageLink": "\/se\/vi-finns-har\/norrkoping",
    "desc": "<p><span>Kromgatan 10\u00a0<\/span><br \/><span>60223 NORRK\u00d6PING<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "14623",
        "street": "Kromgatan",
        "nr": "10",
        "city": "Norrk\u00f6ping",
        "country": "Sverige",
        "postalcode": "602 23",
        "location": "",
        "lat": "58.612874",
        "lng": "16.169157499999983"
    }
}, {
    "name": "Jetpak Oskarshamn",
    "pageLink": "\/se\/vi-finns-har\/oskarshamn",
    "desc": "<p><span>\u00c5sav\u00e4gen 45<br \/><\/span>Box 517<br \/>572 36 Oskarshamn<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "12869",
        "street": "\u00c5sav\u00e4gen",
        "nr": "45",
        "city": "Oskarshamn",
        "country": "Sverige",
        "postalcode": "57236",
        "location": "",
        "lat": "57.24814379999999",
        "lng": "16.43511890000002"
    }
}, {
    "name": "Jetpak Ronneby",
    "pageLink": "\/se\/vi-finns-har\/ronneby",
    "desc": "<p>Bred\u00e5krav\u00e4gen 2<br \/>372 50 Kallinge<\/p>\r\n<p>\u00a0<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "7171",
        "street": "Bred\u00e5krav\u00e4gen",
        "nr": "2",
        "city": "",
        "country": "Sverige",
        "postalcode": "37250",
        "location": "",
        "lat": "56.250522",
        "lng": "15.282167399999935"
    }
}, {
    "name": "Jetpak Skellefte\u00e5",
    "pageLink": "\/se\/vi-finns-har\/skelleftea",
    "desc": "<p><span><br \/><\/span><\/p>\r\n<p><span>931 92 SKELLEFTE\u00c5<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "1008",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Sverige",
        "postalcode": "93192",
        "location": "",
        "lat": "64.624722",
        "lng": "21.076944000000026"
    }
}, {
    "name": "Jetpak Sk\u00f6vde",
    "pageLink": "\/se\/vi-finns-har\/skovde",
    "desc": "<p><span>R\u00e5dmansgatan 24\u00a0<\/span><br \/><span>541 45 Sk\u00f6vde<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "1010",
        "street": "R\u00e5dmansgatan",
        "nr": "24",
        "city": "Sk\u00f6vde",
        "country": "Sverige",
        "postalcode": "54145",
        "location": "",
        "lat": "58.40174629999999",
        "lng": "13.853294499999947"
    }
}, {
    "name": "Jetpak Arlanda",
    "pageLink": "\/se\/vi-finns-har\/arlanda",
    "desc": "<p><span>Sky City<\/span><br \/><span>190 46 Stockholm-Arlanda<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "1012",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Sverige",
        "postalcode": "",
        "location": "",
        "lat": "59.648989",
        "lng": "17.930161"
    }
}, {
    "name": "Jetpak Sundsvall",
    "pageLink": "\/se\/vi-finns-har\/sundsvall",
    "desc": "<p>Norra v\u00e4gen 12<br \/>856 50 Sundsvall<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "13159",
        "street": "Norra V\u00e4gen",
        "nr": "12",
        "city": "Sundsvall",
        "country": "Sverige",
        "postalcode": "856 50",
        "location": "",
        "lat": "62.41696859999999",
        "lng": "17.342408400000068"
    }
}, {
    "name": "Sandane Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/sandane-lufthavn",
    "desc": "<p>Anda<\/p>\r\n<p>6823 Sandane<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5165",
        "street": "",
        "nr": "",
        "city": "",
        "country": "",
        "postalcode": "",
        "location": "",
        "lat": "61.829782",
        "lng": "6.107669"
    }
}, {
    "name": "R\u00f8st Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/rost-lufthavn",
    "desc": "<p>8064 R\u00f8st<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5163",
        "street": "",
        "nr": "",
        "city": "",
        "country": "",
        "postalcode": "",
        "location": "",
        "lat": "67.527383",
        "lng": "12.104745"
    }
}, {
    "name": "Stord Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/stord-lufthavn",
    "desc": "<p>S\u00f8rstokken<\/p>\r\n<p>5400 Stord<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5195",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "5410",
        "location": "",
        "lat": "59.79241",
        "lng": "5.3411590000000615"
    }
}, {
    "name": "Svalbard Lufthavn Longyearbyen",
    "pageLink": "\/no\/her-finnes-vi\/svalbard-lufthavn-longyearbyen",
    "desc": "<p>Longyear<\/p>\r\n<p>9170 Longyearbyen<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5201",
        "street": "",
        "nr": "",
        "city": "Longyearbyen",
        "country": "Svalbard and Jan Mayen",
        "postalcode": "9170",
        "location": "",
        "lat": "78.246084",
        "lng": "15.465562999999975"
    }
}, {
    "name": "Stokmarknes Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/stokmarknes-lufthavn",
    "desc": "<p>Skagen<\/p>\r\n<p>8450 Stokmarknes<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5190",
        "street": "Nedre Skagen",
        "nr": "31",
        "city": "Skagen",
        "country": "Norway",
        "postalcode": "8450",
        "location": "",
        "lat": "68.580593",
        "lng": "15.022865000000024"
    }
}, {
    "name": "Stavanger Lufthavn Sola",
    "pageLink": "\/no\/her-finnes-vi\/stavanger-lufthavn-sola",
    "desc": "<p>Flyplassvegen 230<\/p>\r\n<p>4055 Sola<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5186",
        "street": "Flyplassvegen",
        "nr": "230",
        "city": "",
        "country": "Norway",
        "postalcode": "4055",
        "location": "",
        "lat": "58.880441",
        "lng": "5.63140199999998"
    }
}, {
    "name": "Sandefjord Lufthavn Torp",
    "pageLink": "\/no\/her-finnes-vi\/sandefjord-lufthavn-torp",
    "desc": "<p>Torpveien 130<\/p>\r\n<p>3241 Sandefjord<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5167",
        "street": "Torpveien",
        "nr": "130",
        "city": "",
        "country": "Norway",
        "postalcode": "3241",
        "location": "",
        "lat": "59.182446",
        "lng": "10.256912000000057"
    }
}, {
    "name": "Sandnessj\u00f8en Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/sandnessjoen-lufthavn",
    "desc": "<p>Stokka<\/p>\r\n<p>8800 Sandnessj\u00f8en<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5171",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "8800",
        "location": "",
        "lat": "65.959444",
        "lng": "12.471944000000008"
    }
}, {
    "name": "Sogndal Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/sogndal-lufthavn",
    "desc": "<p>Hauk\u00e5sen<\/p>\r\n<p>5800 Sogndal<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5178",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "6854",
        "location": "",
        "lat": "61.156111",
        "lng": "7.137778000000026"
    }
}, {
    "name": "S\u00f8rkjosen Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/sorkjosen-lufthavn",
    "desc": "<p>Flyplassveien 18<\/p>\r\n<p>9152 S\u00f8rkjosen<\/p>\r\n<p>\u00a0<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5183",
        "street": "",
        "nr": "",
        "city": "",
        "country": "",
        "postalcode": "",
        "location": "",
        "lat": "69.786063",
        "lng": "20.960047"
    }
}, {
    "name": "Jetpak Sveg",
    "pageLink": "\/se\/vi-finns-har\/sveg",
    "desc": "<p><span>\u00d6verberg 3024<\/span><br \/><span>842 94 SVEG<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "1953",
        "street": "",
        "nr": "",
        "city": "\u00d6verberg",
        "country": "Sverige",
        "postalcode": "84294",
        "location": "",
        "lat": "62.07115",
        "lng": "14.301850000000059"
    }
}, {
    "name": "Jetpak Ume\u00e5",
    "pageLink": "\/se\/vi-finns-har\/umea",
    "desc": "<p><span>Ume\u00e5 Airport\u00a0<\/span><br \/><span>904 22 Ume\u00e5<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "2163",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Sweden",
        "postalcode": "",
        "location": "",
        "lat": "63.79356",
        "lng": "20.289795000000026"
    }
}, {
    "name": "Jetpak Uppsala",
    "pageLink": "\/se\/vi-finns-har\/uppsala",
    "desc": "<p>Hytt\u00f6gatan 6<br \/>752 28 Uppsala<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "12860",
        "street": "Hytt\u00f6gatan",
        "nr": "6",
        "city": "Uppsala",
        "country": "Sverige",
        "postalcode": "752 28",
        "location": "",
        "lat": "59.88116379999999",
        "lng": "17.587566400000014"
    }
}, {
    "name": "Jetpak Visby",
    "pageLink": "\/se\/vi-finns-har\/visby",
    "desc": "<p><span>Visby flygplats,\u00a0<\/span><br \/><span>621 41 Visby<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "2176",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Sweden",
        "postalcode": "",
        "location": "",
        "lat": "57.660276",
        "lng": "18.339594000000034"
    }
}, {
    "name": "Jetpak V\u00e4ster\u00e5s",
    "pageLink": "\/se\/vi-finns-har\/vasteras",
    "desc": "<p>Brandthovdagatan 17A<br \/><span>721 35 V\u00e4ster\u00e5s<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "10873",
        "street": "Brandthovdagatan",
        "nr": "17A",
        "city": "V\u00e4ster\u00e5s",
        "country": "Sverige",
        "postalcode": "72135",
        "location": "",
        "lat": "59.6169882",
        "lng": "16.612978099999964"
    }
}, {
    "name": "Jetpak V\u00e4xj\u00f6",
    "pageLink": "\/se\/vi-finns-har\/vaxjo",
    "desc": "<p><span>Arabygatan 51,\u00a0<\/span><br \/><span>352 46 V\u00c4XJ\u00d6<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "2184",
        "street": "Arabygatan",
        "nr": "51",
        "city": "V\u00e4xj\u00f6",
        "country": "Sweden",
        "postalcode": "35246",
        "location": "",
        "lat": "56.8926138",
        "lng": "14.781539399999929"
    }
}, {
    "name": "Jetpak \u00c4ngelholm",
    "pageLink": "\/se\/vi-finns-har\/angelholm",
    "desc": "<p><span>Mogatan 4\u00a0<\/span><br \/><span>254 64 HELSINGBORG<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "3842",
        "street": "Mogatan",
        "nr": "4",
        "city": "Helsingborg",
        "country": "Sweden",
        "postalcode": "25464",
        "location": "",
        "lat": "56.07038799999999",
        "lng": "12.75646400000005"
    }
}, {
    "name": "Jetpak \u00d6rebro",
    "pageLink": "\/se\/vi-finns-har\/orebro",
    "desc": "<p><span>Berglundav\u00e4gen 12 C<\/span><br \/><span>702 36 \u00d6REBRO<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "13215",
        "street": "Berglundav\u00e4gen",
        "nr": "12 C",
        "city": "\u00d6rebro",
        "country": "Sverige",
        "postalcode": "702 28",
        "location": "",
        "lat": "59.23603989999999",
        "lng": "15.139031400000022"
    }
}, {
    "name": "Jetpak \u00d6rnsk\u00f6ldsvik",
    "pageLink": "\/se\/vi-finns-har\/ornskoldsvik",
    "desc": "<p><span>Tegelbruksv\u00e4gen 3\u00a0<\/span><br \/><span>891 55 Arn\u00e4svall<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "13171",
        "street": "Tegelbruksv\u00e4gen",
        "nr": "3",
        "city": "\u00d6rnsk\u00f6ldsvik",
        "country": "Sverige",
        "postalcode": "891 55",
        "location": "",
        "lat": "63.3097454",
        "lng": "18.79239789999997"
    }
}, {
    "name": "Jetpak \u00d6stersund",
    "pageLink": "\/se\/vi-finns-har\/ostersund",
    "desc": "<p><span>Tysj\u00f6v\u00e4gen 8\u00a0<\/span><br \/><span>83152 \u00d6stersund<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "14621",
        "street": "Tysj\u00f6v\u00e4gen",
        "nr": "8",
        "city": "\u00d6stersund",
        "country": "Sverige",
        "postalcode": "831 52",
        "location": "",
        "lat": "63.22244120000001",
        "lng": "14.6402511"
    }
}, {
    "name": "Jetpak Danmark (CPH)",
    "pageLink": "\/dk\/find-jetpak\/jetpak-danmark",
    "desc": "<p>Petersdalvej 1a<span>, port 19<\/span><br \/>2770 Kastrup\u00a0<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "12979",
        "street": "Petersdalvej",
        "nr": "1A, port 19",
        "city": "Kastrup",
        "country": "Danmark",
        "postalcode": "2770",
        "location": "",
        "lat": "55.6203909",
        "lng": "12.676489400000037"
    }
}, {
    "name": "Jetpak Aarhus",
    "pageLink": "\/dk\/find-jetpak\/jetpak-aarhus",
    "desc": "<p>Skar\u00f8v\u00e6nget 3<br \/>8381 Tilst<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4527",
        "street": "Skar\u00f8v\u00e6nget",
        "nr": "35",
        "city": "Tilst",
        "country": "Danmark",
        "postalcode": "8381",
        "location": "",
        "lat": "56.17555119999999",
        "lng": "10.111886499999969"
    }
}, {
    "name": "Aalborg Lufthavn",
    "pageLink": "\/dk\/find-jetpak\/aalborg-lufthavn",
    "desc": "<p>Lufthavnsvej 100<br \/>9400 N\u00f8rresundby<\/p>\r\n<p>+45 9817 4861\u00a0<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "3166",
        "street": "Lufthavnsvej",
        "nr": "100",
        "city": "N\u00f8rresundby",
        "country": "Danmark",
        "postalcode": "9400",
        "location": "",
        "lat": "57.0865634",
        "lng": "9.87154970000006"
    }
}, {
    "name": "Aarhus Lufthavn",
    "pageLink": "\/dk\/find-jetpak\/aarhus-lufthavn",
    "desc": "<p>Stabrandvej 24<br \/>8560 Kolind<\/p>\r\n<p>+45 87757241<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4521",
        "street": "Stabrandvej",
        "nr": "24",
        "city": "Kolind",
        "country": "Danmark",
        "postalcode": "8560",
        "location": "",
        "lat": "56.307842",
        "lng": "10.626727999999957"
    }
}, {
    "name": "Bornholms Lufthavn",
    "pageLink": "\/dk\/find-jetpak\/bornholms-lufthavn",
    "desc": "<p>Sdr. Landevej 2<br \/>3700 R\u00f8nne<\/p>\r\n<p>+45 4178 0444<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "13217",
        "street": "S\u00f8ndre Landevej",
        "nr": "2",
        "city": "R\u00f8nne",
        "country": "Danmark",
        "postalcode": "3700",
        "location": "",
        "lat": "55.069284",
        "lng": "14.749573000000055"
    }
}, {
    "name": "Jetpak Billund ApS",
    "pageLink": "\/dk\/find-jetpak\/jetpak-billund-aps",
    "desc": "<p>Fynsvej 11 A<br \/>7330 Brande<\/p>\r\n<p>Tlf. 30809060<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "13732",
        "street": "Fynsvej",
        "nr": "11 A",
        "city": "Brande",
        "country": "Danmark",
        "postalcode": "7330",
        "location": "",
        "lat": "55.9362825",
        "lng": "9.077837899999963"
    }
}, {
    "name": "Billund Lufthavn",
    "pageLink": "\/dk\/find-jetpak\/billund-lufthavn",
    "desc": "<p>Spirit Air Cargo<br \/>Cargocentervej 63<br \/>7190 Billund<\/p>\r\n<p>+45 3232 1427<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4529",
        "street": "Cargo Centervej",
        "nr": "63",
        "city": "Billund",
        "country": "Danmark",
        "postalcode": "7190",
        "location": "",
        "lat": "55.735493",
        "lng": "9.142057000000023"
    }
}, {
    "name": "S\u00f8nderborg Lufthavn",
    "pageLink": "\/dk\/find-jetpak\/sonderborg-lufthavn",
    "desc": "<p>Lufthavnsvej 2<br \/>6400 S\u00f8nderborg<\/p>\r\n<p>+45 7342 2162<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4537",
        "street": "Lufthavnsvej",
        "nr": "2",
        "city": "S\u00f8nderborg",
        "country": "Danmark",
        "postalcode": "6400",
        "location": "",
        "lat": "54.961675",
        "lng": "9.786951999999928"
    }
}, {
    "name": "Karup Lufthavn",
    "pageLink": "\/dk\/find-jetpak\/karup-lufthavn",
    "desc": "<p>N.O. Hansensvej 4<br \/>7470 Karup<\/p>\r\n<p>+45 9710 0610<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4535",
        "street": "N. O. Hansens Vej",
        "nr": "4",
        "city": "Karup",
        "country": "Danmark",
        "postalcode": "7470",
        "location": "",
        "lat": "56.31061829999999",
        "lng": "9.120631199999934"
    }
}, {
    "name": "Esbjerg Lufthavn",
    "pageLink": "\/dk\/find-jetpak\/esbjerg-lufthavn",
    "desc": "<p>John Tranums Vej 20<br \/>6705 Esbjerg<\/p>\r\n<p>+45 7212 1400<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4533",
        "street": "John Tranums Vej",
        "nr": "20",
        "city": "Esbjerg",
        "country": "Danmark",
        "postalcode": "6705",
        "location": "",
        "lat": "55.52164",
        "lng": "8.549287999999933"
    }
}, {
    "name": "Helsinki-Vantaa",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/helsinki-vantaa",
    "desc": "<p>Rahtitie 1 B<\/p>\r\n<p>01530 Vantaa<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6059",
        "street": "Rahtitie",
        "nr": "1",
        "city": "",
        "country": "Finland",
        "postalcode": "01530",
        "location": "",
        "lat": "60.3111396",
        "lng": "24.97252720000006"
    }
}, {
    "name": "Joensuu",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/joensuu",
    "desc": "<p>Lentoasemantie 30<\/p>\r\n<p>80140 Joensuu<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "12543",
        "street": "Lentoasemantie",
        "nr": "30",
        "city": "",
        "country": "Finland",
        "postalcode": "80400",
        "location": "",
        "lat": "62.65670040000001",
        "lng": "29.61305189999996"
    }
}, {
    "name": "Jyv\u00e4skyl\u00e4",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/jyvaskyla",
    "desc": "<p>Lentoasemantie 40<\/p>\r\n<p>41160 Tikkakoski<\/p>\r\n<p>\u00a0<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6023",
        "street": "Lentoasemantie",
        "nr": "40",
        "city": "",
        "country": "Finland",
        "postalcode": "41160",
        "location": "",
        "lat": "62.4045772",
        "lng": "25.681979100000035"
    }
}, {
    "name": "Kajaani",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/kajaani",
    "desc": "<p>Lentokent\u00e4ntie 7<\/p>\r\n<p>87850 Paltaniemi<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6025",
        "street": "Lentokent\u00e4ntie",
        "nr": "",
        "city": "",
        "country": "Finland",
        "postalcode": "87850",
        "location": "",
        "lat": "64.2812829",
        "lng": "27.669063800000004"
    }
}, {
    "name": "Kemi",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/kemi",
    "desc": "<p>Lentokent\u00e4ntie 75<\/p>\r\n<p>94500 Lautiosaari<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6027",
        "street": "Lentokent\u00e4ntie",
        "nr": "75",
        "city": "",
        "country": "Finland",
        "postalcode": "94200",
        "location": "",
        "lat": "65.7815478",
        "lng": "24.574377400000003"
    }
}, {
    "name": "Kittil\u00e4",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/kittila",
    "desc": "<p>Levintie 259<\/p>\r\n<p>99100 Kittil\u00e4<\/p>\r\n<p>\u00a0<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6029",
        "street": "Levintie",
        "nr": "259",
        "city": "",
        "country": "Finland",
        "postalcode": "99100",
        "location": "",
        "lat": "67.6963331",
        "lng": "24.86324830000001"
    }
}, {
    "name": "Kokkola",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/kokkola",
    "desc": "<p>Lent\u00e4j\u00e4ntie 162<\/p>\r\n<p>68500 Kruunupyy<\/p>\r\n<p>\u00a0<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6033",
        "street": "Lent\u00e4j\u00e4ntie",
        "nr": "162",
        "city": "",
        "country": "Finland",
        "postalcode": "68500",
        "location": "",
        "lat": "63.71872819999999",
        "lng": "23.132933699999967"
    }
}, {
    "name": "Kuopio",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/kuopio",
    "desc": "<p>Lentokent\u00e4ntie 275 C<\/p>\r\n<p>70900 Toivala<\/p>\r\n<p>\u00a0<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6035",
        "street": "Lentokent\u00e4ntie",
        "nr": "275",
        "city": "Siilinj\u00e4rvi",
        "country": "Finland",
        "postalcode": "70900",
        "location": "",
        "lat": "63.00910940000001",
        "lng": "27.787977899999987"
    }
}, {
    "name": "Kuusamo",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/kuusamo",
    "desc": "<p>Lentokent\u00e4ntie<\/p>\r\n<p>93600 Kuusamo<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6037",
        "street": "Lentokent\u00e4ntie",
        "nr": "",
        "city": "",
        "country": "Finland",
        "postalcode": "93600",
        "location": "",
        "lat": "66.0011907",
        "lng": "29.220230000000015"
    }
}, {
    "name": "Maarianhamina",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/maarianhamina",
    "desc": "<p>Flygf\u00e4ltsv\u00e4gen 67<\/p>\r\n<p>22120 Mariehamn<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6041",
        "street": "Flygf\u00e4ltsv\u00e4gen",
        "nr": "",
        "city": "",
        "country": "Aland Islands",
        "postalcode": "22130",
        "location": "",
        "lat": "60.11836890000001",
        "lng": "19.915712999999982"
    }
}, {
    "name": "Oulu",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/oulu",
    "desc": "<p>Lentoasemantie 720<\/p>\r\n<p>90410 Oulu<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6043",
        "street": "Lentokent\u00e4ntie",
        "nr": "720",
        "city": "Oulu",
        "country": "Finland",
        "postalcode": "90410",
        "location": "",
        "lat": "64.930875",
        "lng": "25.35583799999995"
    }
}, {
    "name": "Pori",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/pori",
    "desc": "<p>Lentoasemantie 1<\/p>\r\n<p>28500 Pori<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6045",
        "street": "Lentoasemantie",
        "nr": "1",
        "city": "",
        "country": "Finland",
        "postalcode": "28500",
        "location": "",
        "lat": "61.4702228",
        "lng": "21.793004999999994"
    }
}, {
    "name": "Rovaniemi",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/rovaniemi",
    "desc": "<p>Lentokent\u00e4ntie<\/p>\r\n<p>96100 Rovaniemi<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6047",
        "street": "Lentokent\u00e4ntie",
        "nr": "",
        "city": "Rovaniemi",
        "country": "Finland",
        "postalcode": "96100",
        "location": "",
        "lat": "66.563104",
        "lng": "25.830077999999958"
    }
}, {
    "name": "Savonlinna",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/savonlinna",
    "desc": "<p>Lentoasemantie 50<\/p>\r\n<p>57120 Savonlinna<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6051",
        "street": "Lentoasemantie",
        "nr": "",
        "city": "",
        "country": "Finland",
        "postalcode": "57310",
        "location": "",
        "lat": "61.94265069999999",
        "lng": "28.933253899999954"
    }
}, {
    "name": "Tampere",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/tampere",
    "desc": "<p>Varikontie 14<\/p>\r\n<p>33960 Pirkkala<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6053",
        "street": "Varikontie",
        "nr": "14",
        "city": "",
        "country": "Finland",
        "postalcode": "33960",
        "location": "",
        "lat": "61.42164779999999",
        "lng": "23.61835210000004"
    }
}, {
    "name": "Turku",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/turku",
    "desc": "<p>Lentoasemantie 150<\/p>\r\n<p>20360 Turku<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6055",
        "street": "Lentoasemantie",
        "nr": "150",
        "city": "Turku",
        "country": "Finland",
        "postalcode": "20360",
        "location": "",
        "lat": "60.5110878",
        "lng": "22.27717240000004"
    }
}, {
    "name": "Vaasa",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/vaasa",
    "desc": "<p>Lentokent\u00e4ntie 123<\/p>\r\n<p>65380 Vaasa<\/p>\r\n<p>\u00a0<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6057",
        "street": "Lentokent\u00e4ntie",
        "nr": "123",
        "city": "",
        "country": "Finland",
        "postalcode": "65380",
        "location": "",
        "lat": "63.0442018",
        "lng": "21.751254700000004"
    }
}, {
    "name": "Alta Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/alta-lufthavn",
    "desc": "<p>Altag\u00e5rdsskogen 32<\/p>\r\n<p>9515 Alta<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "1412",
        "street": "Altag\u00e5rdsskogen",
        "nr": "32",
        "city": "",
        "country": "Norway",
        "postalcode": "9515",
        "location": "",
        "lat": "69.97788",
        "lng": "23.346711000000028"
    }
}, {
    "name": "Andenes Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/andenes-lufthavn",
    "desc": "<p>And\u00f8ya<\/p>\r\n<p>8480 Andenes<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "1416",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "8485",
        "location": "",
        "lat": "69.2925",
        "lng": "16.14416700000004"
    }
}, {
    "name": "Bardufoss Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/bardufoss-lufthavn",
    "desc": "<p>9325 Bardufoss<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "1441",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "9325",
        "location": "",
        "lat": "69.055833",
        "lng": "18.540277999999944"
    }
}, {
    "name": "Bergen Lufthavn Flesland",
    "pageLink": "\/no\/her-finnes-vi\/bergen-lufthavn-flesland",
    "desc": "<p>Lilandsveien 162<\/p>\r\n<p>5258 Blomsterdalen<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "13441",
        "street": "Flyplassvegen",
        "nr": "555",
        "city": "Bergen",
        "country": "Norway",
        "postalcode": "5258",
        "location": "",
        "lat": "60.29183",
        "lng": "5.222017000000051"
    }
}, {
    "name": "Jetpak Bergen AS",
    "pageLink": "\/no\/her-finnes-vi\/jetpak-bergen",
    "desc": "<p>Kontorets bes\u00f8ksadresse: <br \/>L\u00f8nningsveien 2, <br \/>5258 Blomsterdalen<\/p>\r\n<p>Henting og levering av pakker: <br \/>Wider\u00f8e Ground Handling, <br \/>Lilandsveien 162, <br \/>5258 Bergen<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "14754",
        "street": "L\u00f8nningsvegen",
        "nr": "2",
        "city": "Bergen",
        "country": "Norway",
        "postalcode": "5258",
        "location": "Flesland",
        "lat": "60.28334119999999",
        "lng": "5.2346727999999985"
    }
}, {
    "name": "Berlev\u00e5g Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/berlevag-lufthavn",
    "desc": "<p>9980 Berlev\u00e5g<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "1447",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "9980",
        "location": "",
        "lat": "70.870036",
        "lng": "29.029759000000013"
    }
}, {
    "name": "Jetpak Malm\u00f6",
    "pageLink": "\/se\/vi-finns-har\/malmo",
    "desc": "<p><span>Travbanegatan 4<\/span><br \/><span>213 77 MALM\u00d6<\/span><\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "14092",
        "street": "Travbanegatan",
        "nr": "4",
        "city": "Malm\u00f6",
        "country": "Sverige",
        "postalcode": "21377",
        "location": "",
        "lat": "55.5694452",
        "lng": "13.073207799999977"
    }
}, {
    "name": "Bod\u00f8 Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/bodo-lufthavn",
    "desc": "<p>Olav V gate 56-60<\/p>\r\n<p>8004 Bod\u00f8<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "7881",
        "street": "Olav V gate",
        "nr": "56-60",
        "city": "",
        "country": "Norway",
        "postalcode": "8004",
        "location": "",
        "lat": "67.268311",
        "lng": "14.362235000000055"
    }
}, {
    "name": "Br\u00f8nn\u00f8ysund Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/bronnoysund-lufthavn",
    "desc": "<p>8900 Br\u00f8nn\u00f8ysund<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "8004",
        "street": "",
        "nr": "",
        "city": "Br\u00f8nn\u00f8ysund",
        "country": "Norway",
        "postalcode": "8900",
        "location": "",
        "lat": "65.461878",
        "lng": "12.216009999999983"
    }
}, {
    "name": "B\u00e5tsfjord Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/batsfjord-lufthavn",
    "desc": "<p>Ordf\u00f8rer Viken vei 1<\/p>\r\n<p>9990 B\u00e5tsfjord<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4814",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "",
        "location": "",
        "lat": "70.6",
        "lng": "29.658999999999992"
    }
}, {
    "name": "Flor\u00f8 Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/floro-lufthavn",
    "desc": "<p>Thor Solbergvei<\/p>\r\n<p>6900 Flor\u00f8<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4818",
        "street": "",
        "nr": "",
        "city": "Flor\u00f8",
        "country": "Norway",
        "postalcode": "6900",
        "location": "",
        "lat": "61.583363",
        "lng": "5.016985999999974"
    }
}, {
    "name": "F\u00f8rde Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/forde-lufthavn",
    "desc": "<p>6977 Bygstad<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4821",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "",
        "location": "",
        "lat": "61.457778",
        "lng": "5.833888999999999"
    }
}, {
    "name": "Hammerfest Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/hammerfest-lufthavn",
    "desc": "<p>9600 Hammerfest<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4823",
        "street": "",
        "nr": "",
        "city": "Hammerfest",
        "country": "Norway",
        "postalcode": "9600",
        "location": "",
        "lat": "70.679722",
        "lng": "23.668611000000055"
    }
}, {
    "name": "Harstad Lufthavn Evenes",
    "pageLink": "\/no\/her-finnes-vi\/harstad-lufthavn-evenes",
    "desc": "<p>8536 Evenes<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4825",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "8536",
        "location": "",
        "lat": "68.491109",
        "lng": "16.68057399999998"
    }
}, {
    "name": "Hasvik Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/hasvik-lufthavn",
    "desc": "<p>9590 Hasvik<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4829",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "9590",
        "location": "",
        "lat": "70.486667",
        "lng": "22.139722000000006"
    }
}, {
    "name": "Haugesund Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/haugesund-lufthavn",
    "desc": "<p>Helganesveien 350<\/p>\r\n<p>4262 Avaldsnes<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "8801",
        "street": "Helganesvegen",
        "nr": "350",
        "city": "",
        "country": "Norway",
        "postalcode": "4262",
        "location": "",
        "lat": "59.34450999999999",
        "lng": "5.21667750000006"
    }
}, {
    "name": "Honningsv\u00e5g Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/honningsvag-lufthavn",
    "desc": "<p>9751 Honningsv\u00e5g<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4836",
        "street": "",
        "nr": "",
        "city": "Honningsv\u00e5g",
        "country": "Norway",
        "postalcode": "9751",
        "location": "",
        "lat": "71.009466",
        "lng": "25.985302000000047"
    }
}, {
    "name": "Kirkenes Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/kirkenes-lufthavn",
    "desc": "<p>9912 Hesseng<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4839",
        "street": "",
        "nr": "",
        "city": "Hesseng",
        "country": "Norway",
        "postalcode": "9912",
        "location": "",
        "lat": "69.722428",
        "lng": "29.88359600000001"
    }
}, {
    "name": "Kristiansand Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/kristiansand-lufthavn",
    "desc": "<p>Kjevik<\/p>\r\n<p>4657 Kjevik<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4843",
        "street": "",
        "nr": "",
        "city": "Kjevik",
        "country": "Norway",
        "postalcode": "4657",
        "location": "",
        "lat": "58.20383",
        "lng": "8.083768999999961"
    }
}, {
    "name": "Kristiansund Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/kristiansund-lufthavn",
    "desc": "<p>Kvernberget<\/p>\r\n<p>6517 Kristiansund<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4846",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "6517",
        "location": "",
        "lat": "63.111944",
        "lng": "7.826110999999969"
    }
}, {
    "name": "Lakselv Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/lakselv-lufthavn",
    "desc": "<p>Banak<\/p>\r\n<p>9700 Lakselv<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4848",
        "street": "",
        "nr": "",
        "city": "Lakselv",
        "country": "Norway",
        "postalcode": "9700",
        "location": "",
        "lat": "70.066267",
        "lng": "24.98210199999994"
    }
}, {
    "name": "Leknes Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/leknes-lufthavn",
    "desc": "<p>Lufthavnveien 29<\/p>\r\n<p>8370 Leknes<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4850",
        "street": "Lufthavnveien",
        "nr": "",
        "city": "Leknes",
        "country": "Norway",
        "postalcode": "8370",
        "location": "",
        "lat": "68.15494799999999",
        "lng": "13.61724240000001"
    }
}, {
    "name": "Mehamn Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/mehamn-lufthavn",
    "desc": "<p>9770 Mehamn<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4852",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "9770",
        "location": "",
        "lat": "71.028729",
        "lng": "27.82606099999998"
    }
}, {
    "name": "Mo i Rana Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/mo-i-rana-lufthavn",
    "desc": "<p>R\u00f8vassdalveien 61<\/p>\r\n<p>8615 Skonseng<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4854",
        "street": "R\u00f8vassdalveien",
        "nr": "61",
        "city": "Skonseng",
        "country": "Norway",
        "postalcode": "8615",
        "location": "",
        "lat": "66.363889",
        "lng": "14.301388999999972"
    }
}, {
    "name": "Molde Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/molde-lufthavn",
    "desc": "<p>\u00c5r\u00f8<\/p>\r\n<p>6421 Molde<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4856",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "6421",
        "location": "",
        "lat": "62.74643",
        "lng": "7.260576000000015"
    }
}, {
    "name": "Mosj\u00f8en Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/mosjoen-lufthavn",
    "desc": "<p>Kj\u00e6rstad<\/p>\r\n<p>8658 Mosj\u00f8en<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4858",
        "street": "",
        "nr": "",
        "city": "Mosj\u00f8en",
        "country": "Norway",
        "postalcode": "8658",
        "location": "",
        "lat": "65.783611",
        "lng": "13.215278000000012"
    }
}, {
    "name": "Moss Lufthavn Rygge",
    "pageLink": "\/no\/her-finnes-vi\/moss-lufthavn-rygge",
    "desc": "<p>Terminalveien 100<\/p>\r\n<p>1580 Rygge<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "4860",
        "street": "Terminalveien",
        "nr": "100",
        "city": "",
        "country": "Norway",
        "postalcode": "1580",
        "location": "",
        "lat": "59.378374",
        "lng": "10.78419699999995"
    }
}, {
    "name": "Namsos Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/namsos-lufthavn",
    "desc": "<p>7800 Namsos<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5130",
        "street": "Flyplassvegen",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "7800",
        "location": "",
        "lat": "64.4744564",
        "lng": "11.576593600000024"
    }
}, {
    "name": "Narvik Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/narvik-lufthavn",
    "desc": "<p>Framnes<\/p>\r\n<p>8500 Narvik<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5138",
        "street": "Flyplassveien",
        "nr": "24",
        "city": "",
        "country": "Norway",
        "postalcode": "8516",
        "location": "",
        "lat": "68.436005",
        "lng": "17.38813700000003"
    }
}, {
    "name": "R\u00f8ros Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/roros-lufthavn",
    "desc": "<p>7374 R\u00f8ros<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5158",
        "street": "Grind-olaveien",
        "nr": "",
        "city": "Roros",
        "country": "Norway",
        "postalcode": "7374",
        "location": "",
        "lat": "62.576515",
        "lng": "11.352482"
    }
}, {
    "name": "R\u00f8rvik Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/rorvik-lufthavn",
    "desc": "<p>Ryum<\/p>\r\n<p>7900 R\u00f8rvik<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "7998",
        "street": "ryum",
        "nr": "",
        "city": "R\u00f8rvik",
        "country": "Norway",
        "postalcode": "",
        "location": "",
        "lat": "64.8620756",
        "lng": "11.237337499999967"
    }
}, {
    "name": "Svolv\u00e6r Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/svolvaer-lufthavn",
    "desc": "<p>Helle<\/p>\r\n<p>8300 Svolv\u00e6r<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5328",
        "street": "",
        "nr": "",
        "city": "Helle",
        "country": "Norway",
        "postalcode": "8300",
        "location": "",
        "lat": "68.243333",
        "lng": "14.669167000000016"
    }
}, {
    "name": "Troms\u00f8 Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/tromso-lufthavn",
    "desc": "<p>Flyplassveien 31<\/p>\r\n<p>9016 Troms\u00f8<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "7883",
        "street": "Flyplassvegen",
        "nr": "31",
        "city": "Troms\u00f8",
        "country": "Norway",
        "postalcode": "9016",
        "location": "",
        "lat": "69.681935",
        "lng": "18.916263999999956"
    }
}, {
    "name": "Trondheim Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/trondheim-lufthavn",
    "desc": "<p>V\u00e6rnes<\/p>\r\n<p>7500 Stj\u00f8rdal<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5332",
        "street": "",
        "nr": "",
        "city": "Stjordal",
        "country": "Norway",
        "postalcode": "7500",
        "location": "",
        "lat": "63.45827",
        "lng": "10.922598999999991"
    }
}, {
    "name": "Vads\u00f8 Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/vadso-lufthavn",
    "desc": "<p>Kiby<\/p>\r\n<p>9800 Vads\u00f8<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5334",
        "street": "",
        "nr": "",
        "city": "Kiby",
        "country": "Norway",
        "postalcode": "",
        "location": "",
        "lat": "70.06222199999999",
        "lng": "29.85722199999998"
    }
}, {
    "name": "Vard\u00f8 Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/vardo-lufthavn",
    "desc": "<p>Svartnes<\/p>\r\n<p>9950 Vard\u00f8<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5336",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "9950",
        "location": "",
        "lat": "70.355278",
        "lng": "31.045000000000073"
    }
}, {
    "name": "V\u00e6r\u00f8y Helikopterhavn",
    "pageLink": "\/no\/her-finnes-vi\/vaeroy-helikopterhavn",
    "desc": "<p>8063 V\u00e6r\u00f8y<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5338",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "",
        "location": "",
        "lat": "67.69084529999999",
        "lng": "12.77662799999996"
    }
}, {
    "name": "\u00d8rland Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/orland-lufthavn",
    "desc": "<p>\u00d8rland Hovedflystasjon<\/p>\r\n<p>7130 Brekstad<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5340",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "7130",
        "location": "",
        "lat": "63.7",
        "lng": "9.616667000000007"
    }
}, {
    "name": "\u00d8rsta-Volda Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/orsta-volda-lufthavn",
    "desc": "<p>Hovden<\/p>\r\n<p>6160 Hovdebygda<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "5342",
        "street": "Torvmyrane",
        "nr": "4",
        "city": "",
        "country": "Norway",
        "postalcode": "6160",
        "location": "",
        "lat": "62.1799414",
        "lng": "6.079501800000003"
    }
}, {
    "name": "\u00c5lesund Lufthavn",
    "pageLink": "\/no\/her-finnes-vi\/alesund-lufthavn",
    "desc": "<p>Flyplassveien 21<\/p>\r\n<p>6040 \u00c5lesund<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "8109",
        "street": "flyplassveien",
        "nr": "21",
        "city": "",
        "country": "Norway",
        "postalcode": "6040",
        "location": "",
        "lat": "62.560598",
        "lng": "6.113761000000068"
    }
}, {
    "name": "Jetpak Eskilstuna",
    "pageLink": "\/se\/vi-finns-har\/eskilstuna",
    "desc": "<p>Box 593\u00a0<br \/>63108 Eskilstuna<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "6608",
        "street": "Bj\u00f6rksgatan",
        "nr": "5",
        "city": "Eskilstuna",
        "country": "Sverige",
        "postalcode": "63221",
        "location": "",
        "lat": "59.3782387",
        "lng": "16.50040910000007"
    }
}, {
    "name": "Estonia",
    "pageLink": "\/fi\/yhteystiedot\/sijaintimme\/estonia",
    "desc": "<p>Jet Express O\u00dc<span>\u00a0<\/span>is providing services in Estonia.<\/p>\r\n<p><a href=\"http:\/\/www.kuller.ee\" target=\"_blank\">www.kuller.ee<\/a><\/p>\r\n<p>\u00a0<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "7240",
        "street": "Lootsi",
        "nr": "3a",
        "city": "Tallinn",
        "country": "Estonia",
        "postalcode": "10151",
        "location": "",
        "lat": "59.4410438",
        "lng": "24.76159580000001"
    }
}, {
    "name": "Jetpak Ltd. - London office",
    "pageLink": "\/uk\/find-jetpak\/jetpak-london",
    "desc": "<p>Handling Agent:\u00a0Courier Facilities Ltd London<br \/>Heathrow Airport<br \/>Building 578<br \/>Sandringham Road London<br \/>Middlesex TW6 3SL\u00a0<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "12545",
        "street": "",
        "nr": "",
        "city": "",
        "country": "United Kingdom",
        "postalcode": "TW6 3SL",
        "location": "",
        "lat": "51.46093579999999",
        "lng": "-0.4748392999999851"
    }
}, {
    "name": "Jetpak Birmingham",
    "pageLink": "\/uk\/find-jetpak\/jetpak-birmingham",
    "desc": "<p>Handling Agent:\u00a0Swissport Cargo Services<br \/>World Cargo center<br \/>Birmingham Airport<br \/>Birmingham B26 3QN<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "11497",
        "street": "",
        "nr": "",
        "city": "Birmingham",
        "country": "United Kingdom",
        "postalcode": "B26 3QN",
        "location": "",
        "lat": "52.4496036",
        "lng": "-1.7516430000000582"
    }
}, {
    "name": "Jetpak Manchester",
    "pageLink": "\/uk\/find-jetpak\/jetpak-manchester",
    "desc": "<p>Handling Agent:\u00a0Swissport Cargo Services<br \/>World Freight Terminal<br \/>Manchester International Airport<br \/>Manchester M90 5UY<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "11499",
        "street": "",
        "nr": "",
        "city": "",
        "country": "United Kingdom",
        "postalcode": "M90 5UY",
        "location": "",
        "lat": "53.3619992",
        "lng": "-2.285597199999984"
    }
}, {
    "name": "Jetpak Aberdeen",
    "pageLink": "\/uk\/find-jetpak\/jetpak-aberdeen",
    "desc": "<p>Handling Agent:\u00a0Swissport Cargo Terminal<br \/>Dunlin Road<br \/>Aberdeen Airport<br \/>Aberdeen AB21 0PB\u00a0<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "11501",
        "street": "",
        "nr": "",
        "city": "Dyce",
        "country": "United Kingdom",
        "postalcode": "AB21 0PB",
        "location": "",
        "lat": "57.2034862",
        "lng": "-2.207505200000014"
    }
}, {
    "name": "Jetpak Amsterdam",
    "pageLink": "\/nl\/find-jetpak\/jetpak-amsterdam",
    "desc": "<p>Handling Agent:<br \/>WFS Holland<br \/>Anchoragelaan 38<br \/>Amsterdam Airport<br \/>1118 LD Schiphol Airport<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "10941",
        "street": "Anchoragelaan",
        "nr": "38",
        "city": "Schiphol",
        "country": "Netherlands",
        "postalcode": "",
        "location": "",
        "lat": "52.2947847",
        "lng": "4.765790400000014"
    }
}, {
    "name": "Jetpak D\u00fcsseldorf",
    "pageLink": "\/nl\/find-jetpak\/jetpak-dusseldorf",
    "desc": "<p>Handling Agent:<br \/>Swissport Cargo Services Deutschland GmbH Frachtstrasse 20<br \/>D\u00fcsseldorf Airport<br \/>D-40474 D\u00fcsseldorf<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "10936",
        "street": "Frachtstra\u00dfe",
        "nr": "20",
        "city": "D\u00fcsseldorf",
        "country": "Germany",
        "postalcode": "40474",
        "location": "",
        "lat": "51.28362449999999",
        "lng": "6.777874300000008"
    }
}, {
    "name": "Jetpak Brussels",
    "pageLink": "\/nl\/find-jetpak\/jetpak-brussels",
    "desc": "<p>Handling Agent:<br \/>Aviapartner Belgium N.V.<br \/>Brucargo Building 703<br \/>Brussels Airport<br \/>1931 Zaventem<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "10939",
        "street": "Brucargo Building ",
        "nr": "703",
        "city": "Zaventem",
        "country": "Belgium",
        "postalcode": "1931",
        "location": "",
        "lat": "50.9078567",
        "lng": "4.457335599999965"
    }
}, {
    "name": "Jetpak Norge AS",
    "pageLink": "\/no\/her-finnes-vi\/jetpak",
    "desc": "<p>Fridtjof Nansensvei\u00a0<\/p>\r\n<p>2060 Gardermoen<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "11165",
        "street": "Fridtjof Nansens veg",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "2060",
        "location": "",
        "lat": "60.1909",
        "lng": "11.0902"
    }
}, {
    "name": "Oslo Lufthavn Gardermoen",
    "pageLink": "\/no\/her-finnes-vi\/oslo-lufthavn-gardermoen",
    "desc": "<p>Jetpak Norge AS<\/p>\r\n<p>Fridtjof Nansensvei\u00a0<\/p>\r\n<p>2060 Gardermoen<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "11163",
        "street": "Fridtjof Nansens veg",
        "nr": "",
        "city": "",
        "country": "Norway",
        "postalcode": "2060",
        "location": "",
        "lat": "60.1909",
        "lng": "11.0902"
    }
}, {
    "name": "Jetpak Jersey",
    "pageLink": "\/uk\/find-jetpak\/jetpak-jersey",
    "desc": "<p>Handling Agent: Oceanair handling Ltd<br \/>The Cargo Centre<br \/>L'Avenue de la Commune,St. Peter<br \/>Jersey JE3 7BY<\/p>\r\n<p>\u00a0<\/p>\r\n<p>\u00a0<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "12971",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Jersey",
        "postalcode": "JE3 7BY",
        "location": "",
        "lat": "49.21257739999999",
        "lng": "-2.2152287000000115"
    }
}, {
    "name": "Jetpak Southampton",
    "pageLink": "\/uk\/find-jetpak\/jetpak-southampton",
    "desc": "<p>Handling Agent: Oceanair Express Logistics<br \/>Unit 2, Cargo Centre<br \/>Mitchell Way,Southampton<br \/>Hampshire SO18 2HG<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "12974",
        "street": "",
        "nr": "",
        "city": "",
        "country": "Storbritannien",
        "postalcode": "SO18 2HG",
        "location": "",
        "lat": "50.9517189",
        "lng": "-1.360892100000001"
    }
}, {
    "name": "Jetpak London City Airport",
    "pageLink": "\/uk\/find-jetpak\/jetpak-london-city-airport",
    "desc": "<p>Handling Agent: Execair Cargo Services<br \/>Unit 1B, Warehouse K<br \/>Seagull Ln<br \/>London E16 1DR,<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "12977",
        "street": "",
        "nr": "",
        "city": "London",
        "country": "Storbritannien",
        "postalcode": "E16 1DR",
        "location": "",
        "lat": "51.5088795",
        "lng": "0.025033500000063214"
    }
}, {
    "name": "Jetpak Trollh\u00e4ttan",
    "pageLink": "\/se\/vi-finns-har\/jetpak-trollhattan",
    "desc": "<p>Batteriv\u00e4gen 8<br \/>461 38 Trollh\u00e4ttan<\/p>\r\n<p>Tel 0520-80 000<\/p>",
    "jpAddress": {
        "error": "",
        "countryFull": null,
        "avID": "13028",
        "street": "Batteriv\u00e4gen",
        "nr": "8",
        "city": "Trollh\u00e4ttan",
        "country": "Sweden",
        "postalcode": "461 38",
        "location": "",
        "lat": "58.2943265",
        "lng": "12.333651299999929"
    }
}];