﻿<?php get_header(); ?>
404
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<section class="bio" id="bio">
			<div class="container">
				<?
				if (function_exists('qtranxf_getLanguage')){ 
					if (qtranxf_getLanguage() == 'en') { ?>
						<h1 style="padding-bottom: 100px">404 - content was not found! Go <a href="/">home</a> instead!</h1><?
					} else if (qtranxf_getLanguage() == 'sv') { ?>
						<h1 style="padding-bottom: 100px">404 - innehållet kunde inte hittas! Gå <a href="/">hem</a> istället!</h1><?
					} 
				} ?>
			</div>
		</section>
	</main>
</div>

<?php get_footer(); ?>